#HMM visualisation
#setup color palette
require("RColorBrewer")
palette(brewer.pal(7,"Accent")[-4])

#read signal
csv_file <- "./1000.txt"
input <- read.csv(csv_file, header=FALSE, sep="\n", quote="\"", dec=".")
x<-input[[1]]
x.len<-length(x)

#read real change points
points_file<-"./1000.pts.txt"
input<-read.csv(points_file, header=F, sep="\n", dec=".")
cp<-input[[1]]

#read Viterbi algorithm result with sumulation parameters
points_file<-"./1000.vit.txt"
input<-read.csv(points_file, header=F, sep="\n", dec=".")
cp.v<-input[[1]]

#read estimation results
points_file<-"./1000.est.txt"
input<-read.csv(points_file, header=F, sep="\n", dec=".")
cp.est<-input[[1]]

#read baum velch
points_file<-"./1000.hmm.txt"
input<-read.csv(points_file, header=F, sep="\n", dec=".")
cp.hmm<-input[[1]]


#draw real agains hmm
par(las=0)
plot(x, type="l")
for(i in 1:length(cp)) {
  rect(cp[i], mean(x), cp[i+1]-1, max(x),border=NA,col=i%%2+1)
}

for(i in 1:length(cp.hmm)) {
  rect(cp.hmm[i], min(x), cp.hmm[i+1]-1, mean(x),border=NA,col=i%%2+3)
}

#draw distribution for both simulated states
plot(function(x) dnorm(x), -4, 4,main = "Normal density", col=2)
curve(dnorm(x, sd=sqrt(1.2)), add=TRUE, col=1)
box()

# histograms for 2 distingiushable and indistinguishible observable variables
require(plotrix)
layout(matrix(c(1,1,2,2), 2, 2, byrow = TRUE))
len <-1000 
l<-list(c(rep(0, len),rep(1,len/3)),c(rep(1, len),rep(0,len/3)))
multhist(l, breaks=2, names.arg=c("False", "True"))
l <- list(rnorm(len),rnorm(len, mean = 0, sd=1.2))
multhist(l, col=c(2,1), mai<-c(2,2,2,2))

logP<-c(-14763.5,-14728.2, -14721.5, -14719.5, -14718.7, -14718.4, -14718.3, 
        -14718.2, -14718.2, -14718.1)
logPd<-logP[1:length(logP)]
par()
plot(append(logPd,-14708.2),type="n")
abline(h=-14708.2, col=1)
lines(logP)

par(las=0)
plot(x, type="l")
for(i in 1:length(cp)) {
  rect(cp[i], mean(x), cp[i+1]-1, max(x),border=NA,col=i%%2+1)
}

for(i in 1:length(cp.v)) {
  rect(cp.v[i], min(x), cp.v[i+1]-1, mean(x),border=NA,col=i%%2+3)
}

lines(x)
box()
par(las=0)
plot(x, type="l")
for(i in 1:length(cp)) {
  rect(cp[i], mean(x), cp[i+1]-1, max(x),border=NA,col=i%%2+1)
}

for(i in 1:length(cp.init)) {
  rect(cp.init[i], min(x), cp.init[i+1]-1, mean(x),border=NA,col=i%%2+3)
}

lines(x)
box()
par(las=0)
