{
    "type_id": "ru.statalgos.hmm.HMMProbParams",
    "num_states": "2",
    "init": "0.9709988999172858",
    "init": "0.02900110008271426",
    "trans": "0.9737603558004508",
    "trans": "0.02623964419954662",
    "trans": "0.02665953017835549",
    "trans": "0.9733404698216489",
    "emit":
    {
        "type_id": "ru.statalgos.hmm.NormalEmitProb",
        "mean": "0.003569460436650512",
        "var": "1.040835164743901",
        "idx": "0"
    },
    "emit":
    {
        "type_id": "ru.statalgos.hmm.NormalEmitProb",
        "mean": "-0.02918625434695378",
        "var": "1.18330689815677",
        "idx": "1"
    }
}
