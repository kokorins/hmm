```{r supplimentary, echo=FALSE, message=FALSE, error=FALSE, warning=FALSE}
require(RColorBrewer)
require(plotrix)
require(functional)

palette(brewer.pal(7,"Accent")[-4])
Tm<-10000 # Max time
M<-2 # Number of states
S<-seq(from=1, to=M) # States [1,2]
I<-rep(1./M, each=M) # Initial distribution [0.5, 0.5]
T<-matrix(c(1-4./Tm, 4./Tm, 4./Tm,1-4./Tm),2) #[0.9995 0.0005; 0.0005 0.9995]
P<-c(Curry("dnorm"), Curry("dnorm",sd=1.2)) # Vector of density functions for each state (N(0,1), N(0,1.21))
G<-c(Curry("rnorm",n=1),Curry("rnorm",n=1, sd=1.2))
Es<-function(P,o) {
  return (sapply(S,function(P,s,o){P[[s]](o)},P=P,o=o))
}
generate<-function(Tm,S,I,T,G) {
  sts<-c()
  obs<-c()
  st<-sample(S, size=1, prob=I, replace=TRUE)
  sts<-append(sts,st)
  obs<-append(obs,G[[st]]())
  for(i in 1:(Tm-1)) {
    st<-sample(S,size=1, prob=T[st,], replace=TRUE)
    sts<-append(sts,st)
    obs<-append(obs,G[[st]]())
  }
  return(cbind(sts,obs))
}
states<-function(S, sts) {
  w<-which(sts==S[1])
  idxs<-which(w[2:length(w)]-w[1:(length(w)-1)]>1)
  cp<-c(0)
  if(w[1]>1) cp<-append(cp, w[1])
  cp<-append(cp,w[idxs]+1)
  cp<-append(cp,w[idxs+1])
  if(w[length(w)]<Tm)cp<-append(cp,w[length(w)]+1)
  cp<-append(cp,Tm)
  return(sort(cp))
}
plotComp<-function(o, cp, cp.tmpl) {
  palette(brewer.pal(7,"Accent")[-4])
  par(las=0)
  plot(o,type='l', xlab="Time", ylab="Value")
  for(i in 1:length(cp)) {
    rect(cp[i], min(o), cp[i+1]-1, mean(o),border=NA,col=i%%2+1)
  }
  for(i in 1:length(cp.tmpl)) {
    rect(cp.tmpl[i], mean(o), cp.tmpl[i+1]-1, max(o),border=NA,col=i%%2+3)
  }
  lines(o)
}
x<-generate(Tm,S,I,T,G)
Q<-x[,1]
O<-x[,2]
cp.tmpl<-states(S, Q)

restoreStates<-function(d,prev_states) {
  idx<-which.max(d)
  s<-c(idx)
  sz<-length(prev_states[1,])
  for(i in sz:2) {
    idx<-prev_states[idx,i]
    s<-c(idx,s)
  }
  return (as.vector(s))
}

viterbiLog<-function(S,I,T,P,O,Tm) {
  logI<-log(I)
  logT<-log(T)
  delta<-logI+log(Es(P,O[1]))
  prev_states<-cbind(rep(0,length(S)))
  for(t in 2:Tm) {
    m<-matrix(rep(delta,length(S)),length(S))+logT
    md<-apply(m,2,max)
    max_delta<-apply(m,2,which.max)
    prev_states<-cbind(prev_states,max_delta)  
    delta<-md+log(Es(P,O[t]))
  }
  return(list(delta=delta,ps=prev_states))
}

alphaLog<-function(S,I,T,P,O,Tm) {
  la<-I*Es(P,O[1])
  coef <-1./sum(la)
  la<-la*coef
  alpha<-cbind(la)
  coefs<-c(coef)
  for(t in 2:Tm) {
    la<-as.vector((la*Es(P,O[t]))%*%T)
    coef<-c(1./sum(la))
    la<-la*coef
    alpha<-cbind(alpha,la)
    coefs<-c(coefs, coef)
  }
  return (list(alpha=alpha, coefs=coefs))
}

betaLog<-function(S,I,T,P,O,Tm, coefs) {
  lb<-rep(1,length(S))
  beta<-cbind(lb)
  for(t in Tm:2) {
    lb<-as.vector(T%*%(Es(P,O[t])*lb))
    lb<-lb*coefs[t]
    beta<-cbind(lb, beta)
  }
  return (beta)
}

xiGamma<-function(t,alpha,beta,S,T,P,O) {
  xi<-matrix(0,nrow=length(S),ncol=length(S))
  denom<-alpha[,t]%*%beta[,t]
  for(sin in S) {
    for(sout in S) {
      nom<-alpha[sin,t]*T[sin,sout]*Es(P,O[t+1])[sout]*beta[sout,t+1]
      xi[sin,sout]<-nom/denom
    }
  }
  return(list(xi=xi,g=rowSums(xi)))
}

XiGammas<-function(Tm,alpha,beta,S,T,P,O) {
  xis<-c()
  gs<-c()
  for(t in 1:(Tm-1)) {
    xg<-xiGamma(t,alpha,beta,S,T,P,O)
    xis<-cbind(xis,as.vector(xg$xi))
    gs<-cbind(gs,as.vector(xg$g))
  }
  return(list(xi=xis,g=gs))
}

NormalReestimation<-function(g,O,S) {
  denom<-rowSums(g)
  mu<-g%*%O/denom
  sigma2<-c()
  for(s in S) {
    s2<-g[s,]%*%(O-mu[s])^2
    sigma2<-c(sigma2,s2)
  }
  return(list(mu=as.vector(mu),sigma2=sigma2/denom))
}

baumWelshLog<-function(S,I,T,P,O,Tm) {
  a<-alphaLog(S,I,T,P,O,Tm)
  b<-betaLog(S,I,T,P,O,Tm,a$coefs)
  
  xg<-XiGammas(Tm, a$alpha,b, S,T,P,O)
  xi<-rowSums(xg$xi)
  dim(xi)<-c(length(S),length(S))
  g<-rowSums(xi)
  new_I <- g/sum(g)

  small_t<-rep(g,length(S))
  dim(small_t)<-c(length(S),length(S))
  new_T <- xi/small_t
  norm_params<-NormalReestimation(xg$g,O[1:(Tm-1)],S)
  new_P<-c()
  for(s in S) {
    m<-norm_params$mu[s]
    force(m)
    s<-norm_params$sigma2[s]
    force(s)
    new_P<-c(new_P,Curry("dnorm",mean=m, sd=sqrt(s)))
  }
  return(list(I=new_I,T=new_T,P=new_P,N=norm_params))  
}
```

# Скрытые цепи Маркова, алгоритм Баума-Велша

## Вступление 

Скрытые модели/цепи Маркова одни из подходов к представлению данных. Мне очень понравилось как обобщается множество таких подходов в [этой статье](http://habrahabr.ru/company/surfingbird/blog/177889/).

В продолжение же моей [предыдущей статьи](http://habrahabr.ru/post/180109/) описания скрытых моделей Маркова, задамся вопросом: откуда взять хорошую модель? Ответ достаточно стандартен, взять неплохую модель и сделать из нее хорошую.

Напомню пример: нам нужно реализовать детектор лжи, который по подрагиванию рук человека, определяет, говорит он правду или нет. Допустим, когда человек лжет, руки трясутся чуть больше, но нам не известно на сколько именно. Возьмем модель наобум, прогоним алгоритм Витерби из предыдущей статьи и получим довольно странные результаты, например:

```{r viterbi_results, echo=FALSE}
PWrong<-c(Curry("dnorm", sd=0.5*sd(O)), Curry("dnorm",sd=1.5*sd(O))) # Vector of density
IWrong<-c(0.5,0.5)
TWrong<-matrix(c(0.999,0.001,0.001,0.999),2)

d<-viterbiLog(S,IWrong,TWrong,PWrong,O,Tm)
rs<-restoreStates(d$delta, d$ps)
cp<-states(S,rs)
plotComp(O,cp,cp.tmpl)
```

Никакого смысла в получившемся разбиении нет; точнее, есть, но не то который хотелось бы видеть. Первый вопрос: как оценить вероятность появления имеющейся выборки в выбранной модели. И второй вопрос, как найти такую модель, в которой имеющаяся выборка была бы наиболее правдоподобна, то есть как оценить качество и как организовать адаптивность к новым данным. Как раз на них и отвечают the forward-backward procedure и the Baum-Welch reestimation procedure.

## Оценочная функция (The forward-backward procedure)

The forward-backward procedure позволяет оценить вероятность того, что в данной модели HMM появится выборка наблюдаемых значений $O=(o_1,\ldots,o_{T_m})$ (Обозначения взяты из предыдущей статьи). Обозначим оценочную функцию как $P(O|HMM)$. И введем две дополнительные функции:

```{r echo=FALSE}
#$$
#P(O|HMM) = P(o_0,\ldots,o_{T_m}|HMM)=\sum_{s\in S} P(o_0,\ldots,o_{T_m},s_{T_m}|HMM)\\

#\sum_{s\in S} P(o_0,\ldots,o_{T_m}, s_{T_m}=s|HMM) = \sum_{s\in S}\left(f_s(o_{T_M})\sum_{s'\in S}P(o_0,\ldots,o_{T_M-1}=s'|HMM) a_{s's}\right).$$

#Обозначив $P(o_0,\ldots,o_t,s_t=s│HMM)=\alpha_t (s)$, получаем рекурентное соотношение:

#$$
#\alpha_{t+1}(s)=f_s(o_{t+1})\sum_{s'\in S} \alpha_t(s')a_{s's},
#$$

#которое обрывается при $t=0$, $\alpha_0 (s)=\pi_s f_s (o_0)$.

#Аналогично, рекуррентно введём обратную последовательность:

#$$
#\beta_t(s) = P(o_{t+1},\ldots,o_{T_m}|s_t=s,HMM) \\
#\beta_{T_m}(s)=1,\\
#\beta_t(s)=\sum_{s'\in S}a_{ss'}f_{s'}(o_{t+1})\beta_{t+1}(s').
#$$
```

```{r alpha}
AlphaFunc<-function(S,I,T,P,O,Tm) {
  la<-I*Es(P,O[1])
  alpha<-cbind(la)
  for(t in 2:Tm) {
    la<-as.vector((la*Es(P,O[t]))%*%T)
    alpha<-cbind(alpha,la)
  }
  return (alpha)
}
```

```{r beta}
BetaFunc<-function(S,I,T,P,O,Tm) {
  lb<-rep(1,length(S))
  beta<-cbind(lb)
  for(t in Tm:2) {
    lb<-as.vector(T%*%(Es(P,O[t])*lb))
    beta<-cbind(lb, beta)
  }
  return (beta)
}
```

Функции $\alpha_t (s)$, $\beta_t (s)$ обладают следующими интересными свойствами:

$$
\sum_{s\in S} \beta_1(s)\pi_s f_s(o_1) = \sum_{s\in S} \alpha_{T_m}(s) = P(O|HMM)=\sum_{s\in S} \alpha_t(s)\beta_t(s),\forall t \in \overline{1,T_m}.
$$

```{r p_o_hmm, echo=FALSE}
PFunc<-function(alpha,Tm) {
  return (sum(a[Tm,]))
}
```

## Процедура уточнения параметров Баума-Велша

Итак, мы уже умеем оценивать появление выборки O в модели HMM и находить последовательность скрытых состояний $Q^*$. Открытым остается вопрос, как найти модель, в которой вероятность появления имеющейся выборки будет максимальной, то есть  $P(O|HMM^*)=\max\limits_{HMM\in\Omega}P(O|HMM)$, где $\Omega$ некоторое множество допустимых моделей. Как утверждается, формального алгоритма нахождения оптимума нет, но зачастую можно хотя бы на капельку приблизиться к "совершенству", за счёт некоторой модификации модели. Именно так и работают итерационные [EM-алгоритмы](http://ru.wikipedia.org/wiki/EM-%E0%EB%E3%EE%F0%E8%F2%EC).

Итак, пусть есть начальная модель $HMM^k$ необходимо найти такую модель $HMM^{k+1}$, что $P(O|HMM^{k+1})\geq P(O|HMM^{k})$. Приступим.

Введем еще две вспомогательные функции:

```{r echo=FALSE}
#$$
#\xi_t(s,s')=\frac{\alpha_t(s)a_{ss'}f_{s'}(o_{t+1})\beta_{t+1}(s')}{\sum_{s\in S}\alpha_t(s)\beta_t(s)}\\
#\gamma_t(s)=\frac{\alpha_t(s)\beta_t(s)}{\sum_{s\in S}\alpha_t(s)\beta_t(s)} = \sum_{s'\in S} \xi(s,s'),
#$$
```

```{r xi_gamma}
xiGamma<-function(t,alpha,beta,S,T,P,O) {
  xi<-matrix(0,nrow=length(S),ncol=length(S))
  denom<-alpha[,t]%*%beta[,t]
  for(sin in S) {
    for(sout in S) {
      nom<-alpha[sin,t]*T[sin,sout]*Es(P,O[t+1])[sout]*beta[sout,t+1]
      xi[sin,sout]<-nom/denom
    }
  }
  return(list(xi=xi,g=rowSums(xi)))
}

XiGammas<-function(Tm,alpha,beta,S,T,P,O) {
  xis<-c()
  gs<-c()
  for(t in 1:(Tm-1)) {
    xg<-xiGamma(t,alpha,beta,S,T,P,O)
    xis<-cbind(xis,as.vector(xg$xi))
    gs<-cbind(gs,as.vector(xg$g))
  }
  return(list(xi=xis,g=gs))
}
```

$\xi(s,s')$ интерпретируется как доля переходов из состояния s в состояние s’ в момент времени t, а  $\gamma_t (s)$  – как общая доля переходов из состояния s в момент времени t.

Подправленные параметры модели $HMM^{k+1}=\langle M^{k+1},S^{k+1},I^{k+1},T^{k+1},E^{k+1}\rangle$ будут иметь следующий вид:

```{r echo=FALSE}
#$$
#M^{k+1}\leftarrow M^{k}\\
#S^{k+1}\leftarrow S^{k}\\
#I^{k+1}=(γ_0 (s),s∈S);\\
#T^{k+1}=\left\|\frac{\sum_{t=0}^{T_m}\xi_t(s,s')}{\sum_{t=0}^{T_m}\gamma_t(s)}, s, s'\in S\right\|\\
#E=(f_s(o\in O),s\in S).
#$$
```


```{r baum_welsh_reestim}
BaumWelshFunc<-function(S,I,T,P,O,Tm) {
  a<-alphaLog(S,I,T,P,O,Tm)
  b<-betaLog(S,I,T,P,O,Tm)
  
  xg<-XiGammas(Tm, a,b, S,T,P,O)
  xi<-rowSums(xg$xi)
  dim(xi)<-c(length(S),length(S))
  g<-rowSums(xi)
  new_I <- g/sum(g)

  small_t<-rep(g,length(S))
  dim(small_t)<-c(length(S),length(S))
  new_T <- xi/small_t
  norm_params<-NormalReestimation(xg$g,O[1:(Tm-1)],S)
  new_P<-c()
  for(s in S) {
    new_P<-c(new_P,Curry("dnorm",mean=norm_params$mu[s], sd=sqrt(norm_params$sigma2[s])))
  }
  return(list(S=S,I=new_I,T=new_T,P=new_P,N=norm_params))  
}
```

Перенастройка функции $f_s(o)$ (NormalReestimation в нашем случае) зависит от конкретной ситуации. Классическая реализация алгоритма предполагает, что наблюдения описываются конечными дискретными распределениями, что нам не подходит. Предлагается использовать следующий метод оценки параметров для наблюдаемых величин:

```{r echo=FALSE}
#а значит, $O=(O_1,\ldots,O_k)$ – конечное множество наблюдаемых значений, и подправленную функцию $f_s(o)$ можно записать следующим образом:

#$$
#f_s(o)=\frac{\sum_{o=O_k,t=0}^{t=T_m}\gamma_t(s)}{\sum_{t=0}^{T_m}\gamma_t(s)}.
#$$
```

```{r normal_reestimation}
NormalReestimation<-function(g,O,S) {
  denom<-rowSums(g)
  mu<-g%*%O/denom
  sigma2<-c()
  for(s in S) {
    s2<-g[s,]%*%(O-mu[s])^2
    sigma2<-c(sigma2,s2)
  }
  return(list(mu=as.vector(mu),sigma2=sigma2/denom))
}
```

# Пример реализации

Рассмотрим задачу о детекторе лжи в более общем варианте. У нас есть общая модель того, как наш детектор лжи реагирует на ложь и искренность субъектов и есть конкретный индивид, который иногда врет иногда говорит правду. Мы хотим настроить нашу модель на работу с этим, конкретным индивидом.

Дано:

1. Модель. Имеются два скрытых состояния, когда сферический человек говорит правду и когда лжет. В том и в другом случае подёргивание рук  можно представить как белый шум, но дисперсия в случае когда человек говорит правду ниже.
2. Выборка из 10000 последовательных наблюдений за нашим индивидом.

Необходимо:

1.	Оценить дисперсию подрагивания рук в случае искренности/лжи этого индивида.
2.  Определить моменты смены этих состояний для него.

Решение:

1. Зададим нашу пятерку:

  1. Число состояний $M\leftarrow 2$;
	2. Состояния $S\leftarrow \{0,1\}$;
	3. Начальное распределение $I\leftarrow(1/2,1/2)$;
	4. По умолчанию предположим, что состояния равноправны и скажем смена происходит в среднем, раз в 999 тактов времени:
$T\leftarrow\left(\begin{matrix}0.999&0.001\\0.001&0.999\end{matrix}\right)$;
	5. $E=(N(0,1/2\cdot\sigma^2 ),N(0,2 \cdot\sigma^2 ))$, где $\sigma^2=var(X)$.
Поясню, что $var(X)$ – это выборочная дисперсия для всей выборки целиком. Начальные оценочные функции для $E_1$,$E_2$ выбираются таким образом, чтобы первое состояние отвечало меньшим значениям дисперсии, а второе – большим. Существенно, что они разные.

2. Будем перенастраивать параметры с помощью алгоритма Баума-Велша до тех пор, пока значение оценочной функции не стабилизируется (об этом ниже).

3. Найдем наиболее правдоподобные скрытые состояния для каждого момента времени с помощью алгоритма Витерби.

Ответ проиллюстрируем: сверху состояния, как они были заданы при моделировании, снизу состояния, которые были выделены с помощью метода скрытых цепей Маркова. 

```{r baum_welsh, echo=FALSE}
con<-c()
nps<-c()
np<-list(I=IWrong,T=TWrong,P=PWrong)
oldP<-0
newP<-1
while(abs(newP-oldP)>1e-1) {
  np<-baumWelshLog(S,np$I,np$T,np$P,O,Tm)
  a<-alphaLog(S, np$I, np$T, np$P, O, Tm)
  oldP<-newP
  newP <- -sum(log(a$coefs))
  con<-c(con,newP)
  nps<-c(nps,np)
}
d<-viterbiLog(S,np$I,np$T,np$P,O,Tm)
rs<-restoreStates(d$delta, d$ps)
cp<-states(S,rs)
plotComp(O,cp,cp.tmpl)
```

На графике ниже приведем значения логарифма оценочной функции в зависимости от числа итераций алгоритма Баума-Велша. Зеленым цветом отмечена оценка, соответствующая параметрам моделирования.

```{r scale, echo=FALSE}
a<-alphaLog(S,I,T,P,O,Tm)
plot(con, type='l', xlab="Iteration", ylab="log(P(O|HMM))")
opt<- -sum(log(a$coefs))
lines(rep(opt, times=length(con)), col="green")
```

## Заключение

Ошибка в оценке скрытых состояний относительно маленькая, но только когда начальное приближение не сильно отличается от того, что мы хотим найти. Нужно понимать, что алгоритм Баума-Велша всего лишь шаг поиска локального оптимума, а значит, может никогда не достигнуть глобального экстремума.