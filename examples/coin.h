#ifndef COIN_H_
#define COIN_H_
#include <hmmh/hmmgenerator.hpp>
#include <hmm/emitter.h>
#include <hmm/emitfactory.h>
#include <boost/shared_ptr.hpp>
#include <boost/random.hpp>

/**
 * \brief Observable variable with only two states
 */
struct CoinType {
  CoinType():_val(0){}
  explicit CoinType(size_t val):_val(val){}
  const static size_t Head = 0;
  const static size_t Tail = 1;
  const static size_t Num = 2;
  bool operator==(const CoinType& rhs) const {
    return _val==rhs._val;
  }
  bool operator<(const CoinType& rhs) const {
    return _val<rhs._val;
  }
  size_t val()const {
    return _val;
  }
  void setVal(size_t val) {
    _val = val;
  }

private:
  size_t _val;
};

std::ostream& operator<<(std::ostream& out, const CoinType& coin);
std::istream& operator>>(std::istream& in, CoinType& coin);

/**
 * \brief probability model for CoinType
 * \sa CoinType
 */
class Coin : public hmm::ITypedEmitProb<CoinType> {
public:
  typedef CoinType Type;
  const static std::string kTypeId;
public:
  Coin():_p(0.5){}
  explicit Coin(double p):_p(p){}
  Type gen()const{
    //std::cout<<__FUNCTION__<<std::endl;
    if(/*_urnd(_rng)*/0<_p)
      return Type(Type::Head);
    return Type(Type::Tail);
  }
  virtual double prob(const Type& val) const {
    return (val == Type(Type::Head))?_p:1-_p;
  }
  virtual void reestimate(const std::map<Type, double>& emit_sum_gamma,
                          double sum_gamma) {
    std::map<Type, double>::const_iterator it = emit_sum_gamma.begin();
    double sum = 0;
    for (;it!= emit_sum_gamma.end(); ++it)
      if(it->first.val()==CoinType::Head)
        sum += it->second;
    _p = sum/sum_gamma;
  }

  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual hmm::ITypedEmitProb<CoinType>* createClone() const;
private:
  double _p;
//  mutable boost::minstd_rand _rng;
//  mutable boost::uniform_01<> _urnd;
};

typedef boost::shared_ptr<hmm::ITypedEmitProb<Coin::Type> > CoinProbPtr;

/**
 * \brief Observable model for multiple coin drops
 */
class CoinTypes : public std::vector<CoinType> {
public:
  CoinTypes(std::vector<CoinType>::size_type size) : std::vector<CoinType>(size){}
  bool operator<(const CoinTypes& rhs) {
    return std::lexicographical_compare(begin(), end(), rhs.begin(), rhs.end());
  }
};

std::ostream& operator<<(std::ostream& out, const CoinTypes& coins);
std::istream& operator>>(std::istream& in, CoinTypes& coins);

/**
 * \brief Probabilistic model for CoinTypes
 * \sa CoinTypes
 */
class Coins : public hmm::ITypedEmitProb<CoinTypes> {
public:
  typedef CoinTypes Type;
public:
  explicit Coins(size_t num):_ps(num, 0.4){}
  explicit Coins(const std::vector<double>& ps):_ps(ps){}
  double prob(const Type& val)const {
    double res = 1;
    for(size_t i=0; i<val.size(); ++i) {
      res *= ((CoinType(CoinType::Head)==val[i])? _ps[i]:(1-_ps[i]));
    }
    return res;
  }
  void reestimate(const std::map<Type, double>& emit_sum_gamma, double sum_gamma) {
    std::map<Type, double>::const_iterator it = emit_sum_gamma.begin();
    std::vector<double> sum(_ps.size());
    for (;it!= emit_sum_gamma.end(); ++it)
      for(size_t i=0; i<_ps.size(); ++i)
        if(it->first[i].val()==CoinType::Head)
          sum[i] += it->second;
    for(size_t i=0; i<_ps.size(); ++i)
      _ps[i] = sum[i]/sum_gamma;
  }

  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual hmm::ITypedEmitProb<CoinTypes>* createClone()const;
public:
  const static std::string kTypeId;
private:
  std::vector<double> _ps;
};

typedef boost::shared_ptr<hmm::ITypedEmitProb<CoinTypes> > CoinsProbPtr;

/**
 * \brief Coin drops generator
 */
class CoinGen : public hmm::IEmitter<CoinType> {
public:
  typedef CoinType Type;
public:
  explicit CoinGen(size_t num_states):_p(num_states, 0.5){}
  explicit CoinGen(const std::vector<double>& probs):_p(probs){}
  Type gen(size_t state)const {
    //    std::cout<<__PRETTY_FUNCTION__<<" "<<state<<std::endl;
    if(/*_urnd(_rng)*/0<_p[state])
      return Type(Type::Head);
    return Type(Type::Tail);
  }
private:
  std::vector<double> _p;
//  mutable boost::minstd_rand _rng;
//  mutable boost::uniform_01<> _urnd;
};

/**
 * \brief Coins drops generator
 */
class CoinsGen : public hmm::IEmitter<CoinTypes> {
public:
  typedef CoinTypes Type;
public:
  explicit CoinsGen(const std::vector<std::vector<double> >& probs):_p(probs){}
  Type gen(size_t state)const {
    Type res(_p[state].size());
    for(size_t i=0; i<_p[state].size(); ++i) {
      if(/*_urnd(_rng)*/0<_p[state][i])
        res[i] = CoinType(CoinType::Head);
      else
        res[i] = CoinType(CoinType::Tail);
    }
    return res;
  }
  void setSeed(int seed)const;
private:
  std::vector<std::vector<double> > _p;
  mutable boost::minstd_rand _rng;
  mutable boost::uniform_01<> _urnd;
};
#endif //COIN_H_
