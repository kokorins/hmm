#include "coin.h"
#include "normal.h"
#include <hmm/emitfactory.hpp>
#include <hmm/enumemitprob.hpp>
#include <hmm/fixedemit.hpp>
#include <hmm/hmmparams.hpp>
#include <hmm/hmm.hpp>
#include <hmmh/hmmgenerator.hpp>
#include <hmm/normalemitprob.h>
#include <hmm/expemitprob.h>
#include <hmmh/hmmgenerator.hpp>
#include <hmmh/commongenerators.h>
#include <cmath>
#include <boost/random.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/assign.hpp>
#include <boost/date_time.hpp>
#include <memory>
#include <vector>
#include <iostream>
#include <set>
#include <map>
#include <numeric>

void testMultipleCoins() 
{
//  size_t num_states = 2;
//  size_t num_coins = 3;
//  std::vector<std::vector<double> > ps(num_states, std::vector<double>(num_coins));
//  ps[0][0] = .9;
//  ps[1][0] = .1;
//  ps[0][1] = .9;
//  ps[1][1] = .1;
//  ps[0][2] = .9;
//  ps[1][2] = .1;
//  CoinsGen coins_gen(ps);
//  hmm::Transition transition(0.1,0.1);
//  StateInitial ci;
//  hmm::Generator<CoinTypes> gen(ci, coins_gen, transition);
//  std::vector<size_t> gen_states;
//  std::shared_ptr<hmm::IValReader<CoinTypes> > sample = gen.generate(1000, &gen_states);
//  std::cout<<"Generated"<<std::endl;
//  hmm::HMMProbParams<CoinTypes> params;
//  params.setNumStates(num_states);
//  std::vector<double> init(num_states, 0.5);
//  params.swapInit(init);
//  std::vector<std::vector<double> > trans(num_states, std::vector<double>(num_states));
//  trans[0][0] = 0.7;
//  trans[1][0] = 0.5;
//  trans[0][1] = 1-trans[0][0];
//  trans[1][1] = 1-trans[1][0];
//  params.swapTrans(trans);
//  std::vector<CoinsProbPtr> emits(num_states);
//  emits[0] = CoinsProbPtr(new Coins(num_coins));
//  emits[1] = CoinsProbPtr(new Coins(num_coins));
//  params.swapEmit(emits);
//  double p;
//  std::vector<size_t> states;
//  //  std::copy(gen_states.begin(), gen_states.end(), std::ostream_iterator<size_t>(std::cout, " "));
//  //  std::cout<<std::endl;
//  //  std::copy(states.begin(), states.end(), std::ostream_iterator<size_t>(std::cout, " "));
//  //  std::cout<<std::endl;
//  params = hmm::HMM::EstimateParameters<CoinTypes>(params, gen_states, *sample);
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  std::cout<<"P before: "<<p<<std::endl;
//  size_t count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i) {
//    if(gen_states[i] == states[i])
//      ++count;
//  }
//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
//  std::cout<<params;
//  for(size_t i=0; i<10; ++i)
//    params = hmm::HMM::LogBaumWelchStep<CoinTypes>(params, *sample);
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  //std::copy(gen_states.begin(), gen_states.end(), std::ostream_iterator<size_t>(std::cout, " "));
//  //std::cout<<std::endl;
//  //std::copy(states.begin(), states.end(), std::ostream_iterator<size_t>(std::cout, " "));
//  //std::cout<<std::endl;
//  std::cout<<"P after: "<<p<<std::endl;
//  count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i) {
//    if(gen_states[i] == states[i])
//      ++count;
//  }
//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
//  std::cout<<params;
}

void testCoin()
{
//  std::vector<double> ps(2);
//  ps[0] = 0.0;
//  ps[1] = 1.0;
//  CoinGen coin_gen(ps);
//  StateChange transition(0.1,0.1);
//  StateInitial ci;
//  hmm::Generator<Coin::Type> gen(ci, coin_gen, transition);
//  std::vector<size_t> gen_states;
//  std::shared_ptr<hmm::IValReader<Coin::Type> > sample = gen.generate(1000, &gen_states);
//  std::cout<<"Generated"<<std::endl;
//  hmm::HMMProbParams<Coin::Type> params;
//  params.setNumStates(2);
//  std::vector<double> init(2, 0.5);
//  params.swapInit(init);
//  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
//  trans[0][0] = 0.9;
//  trans[1][0] = 0.1;
//  trans[0][1] = 1-trans[0][0];
//  trans[1][1] = 1-trans[1][0];
//  params.swapTrans(trans);
//  std::vector<CoinProbPtr> emits(2);
//  emits[0] = CoinProbPtr(new Coin(0.4));
//  emits[1] = CoinProbPtr(new Coin(0.6));
//  params.swapEmit(emits);
//  double p;
//  std::vector<size_t> states;
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  std::cout<<"P before: "<<p<<std::endl;
//  size_t count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i) {
//    if(gen_states[i] == states[i])
//      ++count;
//  }
//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
//  for(size_t i=0; i<10; ++i)
//    params = hmm::HMM::LogBaumWelchStep<Coin::Type>(params, *sample);
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  std::cout<<"P after: "<<p<<std::endl;
//  count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i) {
//    if(gen_states[i] == states[i])
//      ++count;
//  }
//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
////  std::cout<<params;
//  hmm::HMMProbParams<Coin::Type> default_params;
//  default_params.setNumStates(2);
//  std::vector<double> def_init(2, 0.5);
//  default_params.swapInit(def_init);
//  std::vector<std::vector<double> > def_trans(2, std::vector<double>(2));
//  def_trans[0][0] = 0;
//  def_trans[1][0] = 0;
//  def_trans[0][1] = 0;
//  def_trans[1][1] = 0;
//  default_params.swapTrans(def_trans);
//  std::vector<CoinProbPtr> def_emits(2);
//  std::map<Coin::Type, double> enum_map;
//  enum_map.insert(std::make_pair(Coin::Type::Head, 0));
//  enum_map.insert(std::make_pair(Coin::Type::Tail, 0));
//  def_emits[0].reset(new hmm::EnumEmitProb<Coin::Type>(enum_map));
//  def_emits[1].reset(new hmm::EnumEmitProb<Coin::Type>(enum_map));
//  default_params.swapEmit(def_emits);
//  default_params = hmm::HMM::EstimateParameters<Coin::Type>(default_params,
//                                                            gen_states,
//                                                            *sample);
//  std::cout<<default_params;
//  boost::tie(p, states) = hmm::HMM::LogViterbi(default_params, *sample);
//  std::cout<<"Param estimation p: "<<p<<std::endl;
}

void testIndependent() {
  typedef std::vector<size_t> TValueType;
  std::set<TValueType> alphabet;
  alphabet.insert(TValueType(3,0));
  alphabet.insert(TValueType(3,1));
  std::map<TValueType, double> enum_map;
  for(std::set<TValueType>::const_iterator it = alphabet.begin();
      it!=alphabet.end(); ++it) {
    enum_map.insert(std::make_pair(*it, 1.0/alphabet.size()));
  }
  hmm::EnumEmitProb<TValueType> emitter(enum_map);
  TValueType sizes(3,2);
  hmm::FixedEmitProb<TValueType> res_emit = hmm::FixedEmitProb<TValueType>::DependentAsIndependent(emitter, alphabet, sizes);
  {
    const std::vector<std::vector<double> >&probs = res_emit.probs();
    for(size_t i=0; i<probs.size(); ++i) {
      std::cout<<i<<": ";
      std::copy(probs[i].begin(), probs[i].end(), std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
    }
  }
  hmm::EnumEmitProb<TValueType> res2_emit = hmm::EnumEmitProb<TValueType>::IndependentAsDependent(res_emit);
  {
    const std::map<TValueType, double>& enum_map = res2_emit.enumMap();
    for(std::map<TValueType, double>::const_iterator it=enum_map.cbegin(); 
      it!=enum_map.end(); ++it) {
        std::copy(it->first.begin(),it->first.end(), std::ostream_iterator<size_t>(std::cout, " "));
        std::cout<<":"<<it->second<<std::endl;
    }
  }
  alphabet.clear();
  TValueType elem(3);
  for(size_t i=0; i<sizes[0]; ++i) {
    for(size_t j=0; j<sizes[1]; ++j) {
      for(size_t k=0; k<sizes[2]; ++k) {
        elem[0] = i;
        elem[1] = j;
        elem[2] = k;
        alphabet.insert(elem);
      }
    }
  }
  enum_map.clear();
  for(std::set<TValueType>::const_iterator it = alphabet.begin();
      it!=alphabet.end(); ++it) {
    enum_map.insert(std::make_pair(*it, 1.0/alphabet.size()));
  }
  emitter = hmm::EnumEmitProb<TValueType>(enum_map);
  res_emit = hmm::FixedEmitProb<TValueType>::DependentAsIndependent(emitter, alphabet, sizes);
  {
    const std::vector<std::vector<double> >&probs = res_emit.probs();
    for(size_t i=0; i<probs.size(); ++i) {
      std::cout<<i<<": ";
      std::copy(probs[i].begin(), probs[i].end(), std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
    }
  }
  res2_emit = hmm::EnumEmitProb<TValueType>::IndependentAsDependent(res_emit);
  {
    const std::map<TValueType, double>& enum_map = res2_emit.enumMap();
    for(std::map<TValueType, double>::const_iterator it=enum_map.cbegin(); 
      it!=enum_map.end(); ++it) {
        std::copy(it->first.begin(),it->first.end(), std::ostream_iterator<size_t>(std::cout, " "));
        std::cout<<":"<<it->second<<std::endl;
    }
  }

  sizes[0] = 3;
  alphabet.clear();
  for(size_t i=0; i<sizes[0]; ++i) {
    for(size_t j=0; j<sizes[1]; ++j) {
      for(size_t k=0; k<sizes[2]; ++k) {
        elem[0] = i;
        elem[1] = j;
        elem[2] = k;
        alphabet.insert(elem);
      }
    }
  }
  enum_map.clear();
  for(std::set<TValueType>::const_iterator it = alphabet.begin();
    it!=alphabet.end(); ++it) {
      enum_map.insert(std::make_pair(*it, 1.0/alphabet.size()));
  }  
  {
    const std::map<TValueType, double>& enum_map = emitter.enumMap();
    for(std::map<TValueType, double>::const_iterator it=enum_map.cbegin(); 
        it!=enum_map.end(); ++it) {
      std::copy(it->first.begin(),it->first.end(), std::ostream_iterator<size_t>(std::cout, " "));
      std::cout<<":"<<it->second<<std::endl;
    }
  }
  emitter = hmm::EnumEmitProb<TValueType>(enum_map);
  res_emit = hmm::FixedEmitProb<TValueType>::DependentAsIndependent(emitter, alphabet, sizes);
  {
    const std::vector<std::vector<double> >&probs = res_emit.probs();
    for(size_t i=0; i<probs.size(); ++i) {
      std::cout<<i<<": ";
      std::copy(probs[i].begin(), probs[i].end(), std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
    }
  }
  res2_emit = hmm::EnumEmitProb<TValueType>::IndependentAsDependent(res_emit);
  {
    const std::map<TValueType, double>& enum_map = res2_emit.enumMap();
    for(std::map<TValueType, double>::const_iterator it=enum_map.cbegin(); 
      it!=enum_map.end(); ++it) {
        std::copy(it->first.begin(),it->first.end(), std::ostream_iterator<size_t>(std::cout, " "));
        std::cout<<":"<<it->second<<std::endl;
    }
  }
}

void testNormal()
{
//  size_t num_states = 2;
//  std::vector<double> means(num_states);
//  means[1] = 2;
//  std::vector<double> vars(num_states,1);
//  NormalGen norm_gen(means, vars);
//  StateChange transition(0.1,0.1);
//  StateInitial ci;
//  hmm::Generator<double> gen(ci, norm_gen, transition);
//  std::vector<size_t> gen_states;
//  std::shared_ptr<hmm::IValReader<double> > sample = gen.generate(1000, &gen_states);
////  for(int i=0; i<sample->size(); ++i)
////    std::cout<<sample->get(i)<<" ";
////  std::cout<<std::endl;
//  std::cout<<"Generated"<<std::endl;
//  hmm::HMMProbParams<double> params;
//  params.setNumStates(2);
//  std::vector<double> init(2, 0.5);
//  params.swapInit(init);
//  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
//  trans[0][0] = 0.9;
//  trans[1][0] = 0.1;
//  trans[0][1] = 1-trans[0][0];
//  trans[1][1] = 1-trans[1][0];
//  params.swapTrans(trans);
//  std::vector<boost::shared_ptr<hmm::ITypedEmitProb<double> > > emits(2);
//  emits[0] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(0.0,1.0));
//  emits[1] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(2.0,1.0));
//  params.swapEmit(emits);
//  double p;
//  std::vector<size_t> states;
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  std::cout<<"P before: "<<p<<std::endl;
//  size_t count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i)
//    if(gen_states[i] == states[i])
//      ++count;

//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
//  for(size_t i=0; i<10; ++i)
//    params = hmm::HMM::LogBaumWelchStep<double>(params, *sample);
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
//  std::cout<<"P after: "<<p<<std::endl;
//  count = 0;
//  for(size_t i=0; i<gen_states.size(); ++i) {
//    if(gen_states[i] == states[i])
//      ++count;
//  }
//  std::cout<<"ratio: "<<1.*count/gen_states.size()<<std::endl;
////  std::cout<<params;
////  hmm::HMMProbParams<Coin::Type> default_params;
////  default_params.setNumStates(2);
////  std::vector<double> def_init(2, 0.5);
////  default_params.swapInit(def_init);
////  std::vector<std::vector<double> > def_trans(2, std::vector<double>(2));
////  def_trans[0][0] = 0;
////  def_trans[1][0] = 0;
////  def_trans[0][1] = 0;
////  def_trans[1][1] = 0;
////  default_params.swapTrans(def_trans);
////  std::vector<CoinProbPtr> def_emits(2);
////  std::map<Coin::Type, double> enum_map;
////  enum_map.insert(std::make_pair(Coin::Type::Head, 0));
////  enum_map.insert(std::make_pair(Coin::Type::Tail, 0));
////  def_emits[0].reset(new hmm::EnumEmitProb<Coin::Type>(enum_map));
////  def_emits[1].reset(new hmm::EnumEmitProb<Coin::Type>(enum_map));
////  default_params.swapEmit(def_emits);
////  default_params = hmm::HMM::EstimateParameters<Coin::Type>(default_params,
////                                                            gen_states,
////                                                            *sample, 0);
////  std::cout<<default_params;
////  boost::tie(p, states) = hmm::HMM::LogViterbi(default_params, *sample);
////  std::cout<<"Param estimation p: "<<p<<std::endl;
}

//Compare with cusum same model
void testNormalCUSUM()
{
  using namespace hmm;
  boost::minstd_rand urnd;
  urnd.seed(3);
  boost::normal_distribution<> ndist1(0, 1);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen1(urnd, ndist1);
  boost::normal_distribution<> ndist2(0, 1.1);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen2(urnd, ndist2);
  boost::normal_distribution<> ndist3(0, 1);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen3(urnd, ndist3);
  boost::normal_distribution<> ndist4(0, 1.1);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen4(urnd, ndist4);
  size_t len = 10000;
  std::vector<double> ss(len);
  size_t step = len/4;
  std::generate(ss.begin(), ss.begin()+step, ngen1);
  std::generate(ss.begin()+step, ss.begin()+2*step, ngen2);
  std::generate(ss.begin()+2*step, ss.begin()+3*step, ngen3);
  std::generate(ss.begin()+3*step, ss.begin()+4*step, ngen4);
  hmm::HMMProbParams<double> params;
  params.setNumStates(2);
  std::vector<double> init(2, 0.5);
  params.swapInit(init);
  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
  trans[0][0] = 0.99;
  trans[1][0] = 0.01;
  trans[0][1] = 1-trans[0][0];
  trans[1][1] = 1-trans[1][0];
  params.swapTrans(trans);
  std::vector<boost::shared_ptr<hmm::ITypedEmitProb<double> > > emits(2);
  emits[0] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(0,0.5));
  emits[1] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(0.0,3.0));
  params.swapEmit(emits);
  double p;
  std::vector<size_t> states;
  ValReader<double> sample(ss);
  std::vector<double> log_coefs;
  std::vector<std::vector<double> > alphas;
  hmm::HMM::ForwardScaled(params, sample, alphas, log_coefs);
  for(size_t i=0; i<log_coefs.size(); ++i)
    log_coefs[i] =std::log(log_coefs[i]);
  double logP = - std::accumulate(log_coefs.begin(), log_coefs.end(), 0.0);
  alphas.clear();
  log_coefs.clear();
//  boost::tie(p, states) = hmm::HMM::LogViterbi(params, sample);
  std::cout<<logP<<std::endl;
  for(int i=0; i<10; ++i) {
    std::string params_str = params.toString();
    std::cout<<params_str;
    params = hmm::HMM::LogBaumWelchStep(params, sample);
    hmm::HMM::ForwardScaled(params, sample, alphas, log_coefs);
    for(size_t i=0; i<log_coefs.size(); ++i)
      log_coefs[i] =std::log(log_coefs[i]);
    double logP = - std::accumulate(log_coefs.begin(), log_coefs.end(), 0.0);
    alphas.clear();
    log_coefs.clear();
    std::cout<<"log p:"<<logP<<std::endl;
  }
  std::string path = "/home/kokorins/Documents/statalgos/hmm/doc/data/";
  std::string file_name = "Liar_";
  boost::posix_time::ptime t = boost::posix_time::second_clock::local_time();
  file_name += boost::posix_time::to_iso_string(t);
  file_name += "_100.csv";
  std::ofstream fout(path+file_name);
  fout<<"X\n";
  std::copy(ss.begin(), ss.end(), std::ostream_iterator<double>(fout, "\n"));
//  std::cout<<params<<std::endl;
  boost::tie(p, states) = hmm::HMM::LogViterbi(params, sample);
  std::vector<size_t> cp;
  if(states.size()<1)
    return;
  std::cout<<p<<std::endl;
  for(size_t i=1; i<states.size(); ++i)
    if(states[i]!=states[i-1])
      cp.push_back(i);
  std::string json = params.toString();
  std::ofstream fout_params("F:/eclipse/library/hmm/doc/data/1000.params.txt");
  fout_params<<json;
  std::cout<<json;
  std::copy(cp.begin(), cp.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout<<std::endl;
  std::cout<<"Expected: "<<step<<" "<<2*step<<" "<<3*step<<std::endl;
}

//Compare with cusum same model
void testNormalCUSUMFB()
{
  std::string path = "F:/eclipse/library/hmm/doc/data/";
  using namespace hmm;
  using namespace boost::assign;
  size_t len = 10000;
  std::vector<double> init;
  init+=1.0,0.0;
  hmm::StateInitial init_gen(init);
  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
  double trans_p = 4.0/len;
  trans[0][0] = 1-trans_p;
  trans[1][0] = trans_p;
  trans[0][1] = 1-trans[0][0];
  trans[1][1] = 1-trans[1][0];
  hmm::Transition trans_gen(trans);
  std::vector<double> means;
  means += 0.0,0.0;
  std::vector<double> vars;
  vars += 1.0,1.21;
  NormalGen norm_gen(means, vars);
  hmm::Generator<double> gen(init_gen, norm_gen, trans_gen);
  gen.setSeed(0);
  std::vector<size_t> states_exp;
  auto sample = gen.generate(len, &states_exp);
  std::ofstream fout(path+"1000.txt");
  for(size_t i=0; i<sample->size(); ++i)
    fout<<sample->get(i)<<std::endl;
  hmm::HMMProbParams<double> params;
  params.setNumStates(2);
  params.swapInit(init);
  params.swapTrans(trans);
  std::vector<boost::shared_ptr<hmm::ITypedEmitProb<double> > > emits(2);
  emits[0] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(means[0], vars[0]));
  emits[1] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new hmm::NormalEmitProb(means[1], vars[1]));
  params.swapEmit(emits);
  double p;
  std::vector<size_t> states;
  std::vector<double> log_coefs;
  std::vector<std::vector<double> > alphas;
  hmm::HMM::ForwardScaled(params, *sample, alphas, log_coefs);
  for(size_t i=0; i<log_coefs.size(); ++i)
    log_coefs[i] =std::log(log_coefs[i]);
  double logP = - std::accumulate(log_coefs.begin(), log_coefs.end(), 0.0);
  alphas.clear();
  log_coefs.clear();
  std::cout<<"log p:"<<logP<<std::endl;

  boost::tie(p, states) = hmm::HMM::LogViterbi(params, *sample);
  std::vector<size_t> cp;
  if(states.size()<1)
    return;
  std::cout<<p<<std::endl;
  for(size_t i=1; i<states.size(); ++i)
    if(states[i]!=states[i-1])
      cp.push_back(i);
  std::vector<size_t> cp_exp;
  for(size_t i=1; i<states_exp.size(); ++i)
    if(states_exp[i]!=states_exp[i-1])
      cp_exp.push_back(i);
  size_t count = 0;
  for(size_t i=0; i<states_exp.size(); ++i) {
    if(states_exp[i]!=states[i])
      ++count;
  }
  std::cout<<"Num errors: "<<(double)count/len<<std::endl;
  std::string json = params.toString();
  std::ofstream fout_params(path+"v.params.txt");
  fout_params<<json;
  std::ofstream fout_pts(path+"1000.pts.txt");
  std::copy(cp_exp.begin(), cp_exp.end(), std::ostream_iterator<size_t>(fout_pts, "\n"));
  std::ofstream fout_hmm(path+"1000.vit.txt");
  std::copy(cp.begin(), cp.end(), std::ostream_iterator<size_t>(fout_hmm, " "));
}

void testSqr()
{
  using namespace hmm;
  boost::minstd_rand urnd;
  boost::exponential_distribution<> ndist1(0.5);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > ngen1(urnd, ndist1);
  boost::exponential_distribution<> ndist2(2.0);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > ngen2(urnd, ndist2);
  boost::exponential_distribution<> ndist3(1.0);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > ngen3(urnd, ndist3);
  boost::exponential_distribution<> ndist4(2.0);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > ngen4(urnd, ndist4);
  size_t len = 10000;
  std::vector<double> ss(len);
  size_t step = len/4;
  std::generate(ss.begin(), ss.begin()+step, ngen1);
  std::generate(ss.begin()+step, ss.begin()+2*step, ngen2);
  std::generate(ss.begin()+2*step, ss.begin()+3*step, ngen3);
  std::generate(ss.begin()+3*step, ss.begin()+4*step, ngen4);
  hmm::HMMProbParams<double> params;
  params.setNumStates(2);
  std::vector<double> init(2, 0.5);
  params.swapInit(init);
  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
  trans[0][0] = 0.99;
  trans[1][0] = 0.01;
  trans[0][1] = 1-trans[0][0];
  trans[1][1] = 1-trans[1][0];
  params.swapTrans(trans);
  std::vector<boost::shared_ptr<hmm::ITypedEmitProb<double> > > emits(2);
  emits[0] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new ExpEmitProb(0.5));
  emits[1] = boost::shared_ptr<hmm::ITypedEmitProb<double> >(new ExpEmitProb(2.0));
  params.swapEmit(emits);
  double p;
  std::vector<size_t> states;
  ValReader<double> sample(ss);
  for(int i=0; i<10; ++i)
    params = hmm::HMM::LogBaumWelchStep(params, sample);
//  std::cout<<params<<std::endl;
  boost::tie(p, states) = hmm::HMM::LogViterbi(params, sample);
  std::vector<size_t> cp;
  if(states.size()<1)
    return;
  for(size_t i=1; i<states.size(); ++i)
    if(states[i]!=states[i-1])
      cp.push_back(i);
  std::copy(cp.begin(), cp.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout<<std::endl;
  std::cout<<"Expected: "<<step<<" "<<2*step<<" "<<3*step<<std::endl;
}

int main() 
{
//  testCoin();
//  testMultipleCoins();
//  testIndependent();
//  testNormal();
  testNormalCUSUM();
//  testNormalCUSUMFB();
//  testSqr();
  return 0;
}
