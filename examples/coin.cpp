#include "coin.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

const std::string Coin::kTypeId = "statalgos.hmm.Coin";
const std::string Coins::kTypeId = "statalgos.hmm.Coins";

const std::string &Coin::type() const
{
  return kTypeId;
}

std::string Coin::toString() const
{
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("p", _p);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool Coin::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId)
    return false;
  _p  = pt.get<double>("p");
  return true;
}

hmm::ITypedEmitProb<CoinType> *Coin::createClone() const
{
  return new Coin(*this);
}


const std::string &Coins::type() const
{
  return kTypeId;
}

std::string Coins::toString() const
{
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  for(size_t i=0; i<_ps.size(); ++i)
    pt.add("ps", _ps[i]);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool Coins::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId)
    return false;
  _ps.clear();
  BOOST_FOREACH(ptree::value_type &v, pt.get_child("ps")) {
    std::stringstream ss;
    ss<<v.second.data();
    double va;
    ss>>va;
    _ps.push_back(va);
  }
  return true;
}

hmm::ITypedEmitProb<CoinTypes> *Coins::createClone() const
{
  return new Coins(*this);
}

std::ostream& operator<<(std::ostream& out, const CoinTypes& coins) {
  out<< coins.size();
  for(size_t i=0; i<coins.size(); ++i)
    out<<coins[i]<<" ";
  return out;
}

std::istream& operator>>(std::istream& in, CoinTypes& coins) {
  size_t sz, v;
  in>>sz;
  for(size_t i=0; i<sz; ++i)  {
    in>>v;
    coins.push_back(CoinType(v));
  }
  return in;
}

std::ostream& operator<<(std::ostream& out, const CoinType& coin) {
  out<<coin.val();
  return out;
}

std::istream& operator>>(std::istream& in, CoinType& coin) {
  size_t v;
  in>>v;
  coin.setVal(v);
  return in;
}

void CoinsGen::setSeed(int seed) const
{
  _rng.seed(seed);
}
