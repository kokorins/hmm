#ifndef NORMAL_H
#define NORMAL_H
#include <hmmh/hmmgenerator.hpp>
//#include <hmm/emitfactory.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random.hpp>

/**
 * \brief Generator for normaly distributed values
 */
class NormalGen : public hmm::IEmitter<double> {
public:
  typedef double Type;
public:
  NormalGen(const std::vector<double>& means,
            const std::vector<double>& vars) {
    for(size_t i=0; i<means.size(); ++i) {
      boost::normal_distribution<>d(means[i],sqrt(vars[i]));
      _nrnds.push_back(boost::shared_ptr<boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > >(
                         new boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> >(_rng, d)));
    }
  }

  Type gen(size_t state)const {
    return (*_nrnds[state])();
  }
  void setSeed(int seed) const {
    _rng.seed(seed);
  }
private:
  mutable boost::minstd_rand _rng;
  mutable std::vector<boost::shared_ptr<boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > > >_nrnds;
};

#endif // NORMAL_H
