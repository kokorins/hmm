#ifndef HMMGENERATOR_HPP_
#define HMMGENERATOR_HPP_
#include "hmmgenerator.h"

namespace hmm {
template <typename ValueType>
std::shared_ptr<IValReader<ValueType> >
Generator<ValueType>::generate(size_t num, std::vector<size_t>* states /*= 0*/) const
{
  size_t state;
  std::vector<ValueType> res(1, first(state));
  if(states) {
    states->clear();
    states->reserve(num);
    states->push_back(state);
  }

  res.reserve(num);
  for(;num>1; --num) {
    res.push_back(next(state));
    if(states) {
      states->push_back(state);
    }
  }
  return std::shared_ptr<IValReader<ValueType> >(new ValReader<ValueType>(res));
}

template <typename ValueType>
void Generator<ValueType>::setSeed(int seed)const
{
  _init.setSeed(seed);
  _emitter.setSeed(seed);
  _trans.setSeed(seed);
}
} // namespace hmm

#endif /* HMMGENERATOR_HPP_ */
