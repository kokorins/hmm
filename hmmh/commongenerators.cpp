#include "commongenerators.h"
#include <chen/chen.h>
namespace hmm {
Transition::Transition(const std::vector<std::vector<double> >& trans)
{
  for(size_t i=0; i<trans.size(); ++i)
    _gens.push_back(std::make_shared<chen::Chen>(trans[i]));
}

size_t Transition::gen(size_t cur_idx) const
{
  return _gens[cur_idx]->next();
}

void Transition::setSeed(int seed) const
{
  for(size_t i=0; i<_gens.size(); ++i)
    _gens[i]->setSeed(seed);
}

StateInitial::StateInitial(const std::vector<double> &p): _gen(new chen::Chen(p)) {}

size_t StateInitial::gen() const
{
  return _gen->next();
}

void StateInitial::setSeed(int seed) const
{
  _gen->setSeed(seed);
}

} //namespace hmm
