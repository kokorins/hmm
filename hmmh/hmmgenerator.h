#ifndef HMMGENERATOR_H_
#define HMMGENERATOR_H_
#include <hmm/hmmmanager.h>
#include <vector>
#include <memory>
/**
 * \file hmmgenerator.h "hmmh/hmmgenerator.h"
 * This file includes the generator class to test the different models for HMM algorithms.
 * The client code should provide:
 *  - vector for initial distribution,
 *  - matrix for states transition,
 *  - generators for observed variables.
 */

namespace hmm {
template <typename ValueType>
/**
 * \interface IEmitter hmmgenerator.h "hmmh/hmmgenerator.h"
 * An interface for observablie variables
 */
class IEmitter {
public:
  virtual ~IEmitter(){}
  /**
   * \brief the observable variable generator
   * This fuction should get the current state and generate the observable value of a given type.
   * Note that some HMM models is sharpen for special properties of ValueType
   * \param state current state
   * \return observable value
   */
  virtual ValueType gen(size_t /*state*/)const = 0;
  /**
   * \brief Set seed for random generator
   */
  virtual void setSeed(int seed)const=0;
};

/**
 * \interface IInit hmmgenerator.h "hmmh/hmmgenerator.h"
 * Interface for initial state generation
 */
class IInit {
public:
  virtual ~IInit(){}
  /**
   * \brief generates the initial state
   * \return initial state
   */
  virtual size_t gen()const=0;
  virtual void setSeed(int seed)const=0;
};

/**
 * \interface ITransition hmmgenerator.h "hmmh/hmmgenerator.h"
 * Generates the current transition between the states
 */
class ITransition {
public:
  virtual ~ITransition(){}
  /**
   * \brief interface for current state transition generation
   * \param cur_state current state to transit from.
   * \return new states to move to, it could be the same state as on previous step.
   */
  virtual size_t gen(size_t /*cur_state*/)const=0;
  virtual void setSeed(int seed)const=0;
};

/**
 * \class Generator hmmgenerator.h "hmmh/hmmgenerator.h"
 * \brief Simple straightforward HMM generator.
 * This generator is created for testing purposes.
 */
template <typename ValueType>
class Generator {
public:
  /**
   * \brief the Initial position, emitter function and transition matrix should be
   * defined
   * \param init the interface for initial state distribution
   * \param emitter the interface for all states observation emition probabilities
   * \param trans interface for transition matrix implementation
   */
  Generator(const IInit& init, const IEmitter<ValueType>& emitter,
            const ITransition& trans):
    _init(init), _emitter(emitter), _trans(trans) {}

  /**
   * \brief creates the array of observation according to rules
   * in constructor Generator()
   * \param num number of observations to be generated
   * \param states if states non null pointer, it would contain the list
   * of hidden states used for a generation of observations.
   * \return a pointer to IValReader<ValueType> interface
   * \sa IValReader<ValueType>
   */
  std::shared_ptr<IValReader<ValueType> > generate(size_t num, std::vector<size_t>* states = 0) const;
  void setSeed(int seed)const;
private:
  ValueType first(size_t& state) const {
    state = _init.gen();
    return _emitter.gen(state);
  }

  ValueType next(size_t& state) const {
    state = _trans.gen(state);
    return _emitter.gen(state);
  }

private:
  const IInit& _init;
  const IEmitter<ValueType>& _emitter;
  const ITransition& _trans;
};

} // namespace hmm
#endif /* HMMGENERATOR_H_ */
