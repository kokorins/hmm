#ifndef COMMONGENERATORS_H
#define COMMONGENERATORS_H
#include "hmmgenerator.h"
#include <vector>
#include <memory>
/// \file commongenerators.h "hmmh/commongenerators.h"
namespace chen {
  class Chen;
}
namespace hmm {
/**
 * \brief States transition according to transition matrix
 */
class Transition : public hmm::ITransition {
public:
  Transition(const std::vector<std::vector<double> >& trans);
  virtual size_t gen(size_t cur_idx) const;
  virtual void setSeed(int seed) const;
private:
  std::vector<std::shared_ptr<chen::Chen> > _gens;
};

/**
 * Generation of initial states distribution
 */
class StateInitial : public hmm::IInit {
public:
  explicit StateInitial(const std::vector<double>&p);
  size_t gen() const;
  virtual void setSeed(int seed)const;
private:
  std::shared_ptr<chen::Chen> _gen;
};

}//namespace hmm
#endif // COMMONGENERATORS_H
