#ifndef EMITTER_H_
#define EMITTER_H_

#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>

/// \file emitter.h
namespace hmm {
/**
 * \interface IEmitProb emitter.h "hmm/emitter.h"
 * \brief Implements a function for emission an observation for given state.
 */
template <typename ValueType>
class IEmitProb {
public:
  virtual ~IEmitProb() {}
  /**
   * \brief probability or density.
   *
   * This function should return a probability or density of
   * a current value of observation for current model.
   * Supposed to be a part of vector to match the probabilities
   * for each hidden state
   * \param val the value which has been observed
   * \return the probability or density of model in a given point
   */
  virtual double prob(const ValueType& val)const=0;
  /**
   * \brief Interface for function initial estimation and reestimation.
   * \param emit_sum_gamma is map of values key is a value which has been occure, and value is a number of times it has been occur
   * \param sum_gamma total number of occurance
   */
  virtual void reestimate(const std::map<ValueType, double>& emit_sum_gamma, double sum_gamma) = 0;
};

/**
 * \interface IEmitProb emitter.h "hmm/emitter.h"
 * \brief Implements a function for emission an observation for given state.
 */
template <typename ValueType>
class ITypedEmitProb : public IEmitProb<ValueType> {
public:
  /**
   * type id of a class, this property should be implemented
   * in each class and should identify each class.
   * \return statalgos.hmm.[classname]
   */
  virtual const std::string& type()const = 0;
  /**
   * Translates class to string
   */
  virtual std::string toString()const=0;
  /**
   * Set parameters from string if correct format
   * \return false if any troubles or mistakes
   */
  virtual bool fromString(const std::string& json)=0;
  virtual ITypedEmitProb* createClone()const = 0;
};
} // namespace hmm

#endif /* EMITTER_H_ */
