#ifndef HMMPARAMS_H_
#define HMMPARAMS_H_
#include "emitter.h"
#include "emitfactory.h"
#include <vector>
#include <cstddef>
#include <boost/shared_ptr.hpp>
/// \file hmmparams.h
namespace hmm {

/**
 * \brief Encapsulates all possible parameters of HMM.
 */
template <typename ValueType>
class HMMProbParams {
public:
  typedef boost::shared_ptr<ITypedEmitProb<ValueType> > TEmitPtr;
  const static std::string kTypeId;
public:
  /**
   * Set number of states if hidden Markov process
   */
  void setNumStates(size_t num_states);
  /**
   * Set the initial probabilities of each hidden state
   * \param init should be a length of num_states
   */
  void swapInit(std::vector<double>& init);
  /**
   * Set the transiotion probabilities matrix
   * \param trans should be of the length num_states*num_states
   */
  void swapTrans(std::vector<std::vector<double> >& trans);
  /**
   * Set the emition probability function for each state
   * \param emit should be of the length num_states
   * TEmitPtr should cover all possible values of Alphabet(EnumType<ValueType>())
   * if ValueType is enum, or be a density function if ValueType is continuous
   */
  void swapEmit(std::vector<TEmitPtr>& emit);
  /**
   * \return number of hidden states
   */
  size_t numStates()const;
  /**
   * \return initial states distribution
   */
  const std::vector<double>& init()const;
  /**
   * \return probabilities transition matrix
   */
  const std::vector<std::vector<double> >& trans()const;
  /**
   * \return emission probabilities.
   */
  const std::vector<TEmitPtr>& emits() const;
  const std::string& type() const;
  std::string toString() const;
  bool fromString(const std::string& json, const IEmitFactory<ValueType> &emit_factory);
private:
  size_t _num_states;
  std::vector<double> _init;
  std::vector<std::vector<double> > _trans;
  std::vector<TEmitPtr> _emit;
};

template <typename ValueType>
std::ostream& operator<<(std::ostream& out,
                         const HMMProbParams<ValueType>& params);
} // namespace hmm
#endif /* HMMPARAMS_H_ */
