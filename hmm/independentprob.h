#ifndef INDEPENDENTPROB_H
#define INDEPENDENTPROB_H
#include "emitter.h"
#include "emitfactory.h"
#include <vector>
#include <ostream>
#include <boost/shared_ptr.hpp>
namespace hmm {
typedef double ValueType;
/**
 * \class IndependentProb independentprob.h "hmm/independentprob.h"
 * \brief Interface for handling a number of independent variables of a same type
 * \todo Tuple is more appropriate for the case, but still there is no in a standard
 */
class IndependentProb : public ITypedEmitProb<std::vector<ValueType> > {
public:
  const static std::string kTypeId;
  typedef boost::shared_ptr<ITypedEmitProb<ValueType> > EmitProbPtr;
public:
  IndependentProb();
  explicit IndependentProb(const std::vector<EmitProbPtr>& emitters);
  virtual double prob(const std::vector<ValueType>& val)const;
  virtual void reestimate(const std::map<std::vector<ValueType>, double>& emit_sum_gamma,
                          double sum_gamma);
  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual ITypedEmitProb<std::vector<ValueType> >* createClone()const;
  void setEmitFactory(IEmitFactory<ValueType>* emit_fact);
  void swapEmitters(std::vector<EmitProbPtr>& emitters);
  const std::vector<EmitProbPtr>& emitters()const;
private:
  std::vector<EmitProbPtr> _emitters;
  boost::shared_ptr<IEmitFactory<ValueType> > _emit_fact;
};
} //namespace hmm
#endif // INDEPENDENTPROB_H
