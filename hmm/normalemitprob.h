#ifndef NORMALEMITPROB_H
#define NORMALEMITPROB_H
#include "emitter.h"
#include <string>
namespace hmm {
/**
 * \class NormalEmitProb normalemitprob.h "hmm/normalemitprob.h"
 * \brief Normal distribution state model.
 *
 * \sa HMM::BaumWelchStep()
 */
class NormalEmitProb : public ITypedEmitProb<double> {
public:
public:
  NormalEmitProb() : _mean(0), _var(1) {}
  NormalEmitProb(double mean, double var): _mean(mean),_var(var) {}
  /**
   * \param val ValueType is a double value
   * \return density at a point
   */
  virtual double prob(const double& val)const;
  virtual void reestimate(const std::map<double, double>& emit_sum_gamma,
                          double sum_gamma);
  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual ITypedEmitProb<double>* createClone()const;
  double mean()const;
  double var()const;
public:
  static const std::string kTypeId;
private:
  double _mean;
  double _var;
};
}//namespace hmm
#endif // NORMALEMITPROB_H
