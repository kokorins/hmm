#ifndef EXPEMITPROB_H
#define EXPEMITPROB_H
#include "emitter.h"
#include <string>

namespace hmm {
/**
 * \class ExpEmitProb expemitprob.h "hmm/expemitprob.h"
 * \brief Exponential distribution state model.
 *
 * \sa HMM::BaumWelchStep()
 */
class ExpEmitProb : public ITypedEmitProb<double> {
public:
  const static std::string kTypeId;
public:
  ExpEmitProb() : _lambda(1) {}
  explicit ExpEmitProb(double lambda): _lambda(lambda) {}
  /**
   * \param val value
   * \return density at a point
   */
  virtual double prob(const double& val)const;
  virtual void reestimate(const std::map<double, double>& emit_sum_gamma,
                          double sum_gamma);
  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual ITypedEmitProb<double>* createClone()const;
public:
  /**
   * \return parameter for model
   */
  double lambda()const;
private:
  double _lambda;
};
} //namespace hmm
#endif // EXPEMITPROB_H
