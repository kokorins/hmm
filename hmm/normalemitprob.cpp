#include "normalemitprob.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace hmm {
const std::string NormalEmitProb::kTypeId = "statalgos.hmm.NormalEmitProb";
double NormalEmitProb::prob(const double& val)const
{
  const static double pi = 3.1415926535;
  double nom = (val-_mean);
  nom *= nom;
  double d = std::exp(-nom/(2*_var));
  d /= std::sqrt(2*pi*_var);
  return d;
}

void NormalEmitProb::reestimate(const std::map<double, double>& emit_sum_gamma,
                                double sum_gamma)
{
  _mean = 0;
  std::map<double, double>::const_iterator it = emit_sum_gamma.begin();
  for(;it!=emit_sum_gamma.end(); ++it)
    _mean += it->first*it->second;
  _mean /= sum_gamma;
  _var = 0;
  it = emit_sum_gamma.begin();
  for(;it!=emit_sum_gamma.end(); ++it) {
    double elem = it->first-_mean;
    _var += it->second*elem*elem;
  }
  _var /= sum_gamma;
}

const std::string &NormalEmitProb::type() const
{
  return kTypeId;
}

std::string NormalEmitProb::toString() const
{
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("mean", _mean);
  pt.put("var", _var);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool NormalEmitProb::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get_value("type_id");
  if(type_str!=kTypeId)
    return false;
  _mean  = pt.get<double>("mean");
  _var = pt.get<double>("var");
  return true;
}

ITypedEmitProb<double> *NormalEmitProb::createClone() const
{
  return new NormalEmitProb(*this);
}

double NormalEmitProb::mean() const
{
  return _mean;
}

double NormalEmitProb::var() const
{
  return _var;
}
}//namespace hmm
