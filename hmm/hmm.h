#ifndef HMM_H_
#define HMM_H_
#include "hmmmanager.h"
#include "hmmparams.h"
#include "fixedemit.h"
#include "enumemitprob.h"
#include <set>
/**
 * \file hmm.h
 * \mainpage "Hidden Markov Models algorithms"
 * Includes calculation of a path probability.
 * Search for maximum likelihood path with given output: Viterbi algorithm.
 * Parameters optimization according to Baum-Walsh
 * Hidden Markov Model algorithms implementation according to
 * A tutorial on Hidden Markov Models and Selected applications in Speech Recognition
 * Lawrence R. Rabiner. Proceedings of the IEEE, vol 77, No 2, February 1989
 *
 */
namespace hmm {
/**
 * \class HMM hmm.h "hmm/hmm.h"
 * \brief The static class with a set of different algorithms related to HMM
 */
class HMM {
  typedef std::vector<double> TVector;
  typedef std::vector<TVector> TMatrix;
  typedef std::vector<size_t> TStates;
public:
  /**
   * \brief scaled alphas calculation
   *
   * Calculates alphas and coefficients for their normalization, which should be 
   * applied to betas.
   * \param params parameters of HMM
   * \param sample observed sample
   * \param alphas output alphas
   * \param log_coefs coefficient of scaling for each timestamp
   * \sa Forward()
   */
  template <typename ValueType>
  static void ForwardScaled(const HMMProbParams<ValueType>& params, const IValReader<ValueType>& sample,
                            TMatrix& alphas, TVector& log_coefs);

  /**
   * \brief Calculates scaled beta values
   * \param params hmm properties
   * \param sample observation sample
   * \param coefs vector of alphas scaling coefficients
   * \param betas resulting vector of betas
   * \sa Forward()
   */
  template <typename ValueType>
  static void
  BackwardScaled(const HMMProbParams<ValueType>& params,
                 const IValReader<ValueType>& sample,
                 const TVector& coefs,
                 TMatrix & betas);

  /**
   * \brief Calculates probability score logP and vector of maximum probability Hidden states.
   * Initial parameters, transition matrix and emission probabilities are supposed to
   * be known.
   * Initial parameters, transition matrix and emission probabilities are supposed to
   * be known.
   * ValueType is a type of observations
   * \param params parameters of HMM: initial distribution, transition matrix,
   * emission functions.
   * \param sample observed sample
   * \return the pair with score and vector of hidden states. The score shows the
   * probability of best sequence of hidden states to show such observations.
   * The hidden states is labeled with numbers 0, num_states-1
   */
  template <typename ValueType>
  static std::pair<double, TStates >
  LogViterbi(const HMMProbParams<ValueType>& params, const IValReader<ValueType>& sample);


  /**
   * \brief estimates parameters by known states and observations
   *
   * States and sample should have same size.
   * \param num_states information about number of hidden states.
   * \param emits types of observable distributions. emits.size()==num_states
   * \param states is a vector of training hidden Markov states. states.unique()<=num_states
   * \param sample is a vector of observation for given HMM.
   * \return estimated parameters
   */
  template <typename ValueType>
  static HMMProbParams<ValueType>
  EstimateParameters(size_t num_states,
                     std::vector<typename HMMProbParams<ValueType>::TEmitPtr>& emits,
                     const TStates& states,
                     const IValReader<ValueType>& sample);

  /**
   * \brief Algorithm for parameters calibration which goes to the position of better
   * probability to match observations with current model
   * The observation alphabet is supposed to be finite discrete set.
   * \param params initial parameters of HMM.
   * \param sample observed sample
   * \param alphabet of observed values
   * \return a new set of HMM parameters after a step of Baum-Welch algorithm.
   */
  template <typename ValueType>
  static HMMProbParams<ValueType>
  LogBaumWelchStep(const HMMProbParams<ValueType>& params,
                   const IValReader<ValueType>& sample);

private:
  template<typename ValueType>
  static TVector
  EstimateInit(size_t num_states,
               const TStates& states,
               const IValReader<ValueType>& sample);

  template<typename ValueType>
  static TMatrix
  EstimateTrans(size_t num_states,
                const TStates& states,
                const IValReader<ValueType>& sample);

  template <typename ValueType>
  static void CreateEmitters(const std::vector<std::map<ValueType, double> >& emit_sum_gamma,
                             const TVector& sum_gamma,
                             std::vector<boost::shared_ptr<ITypedEmitProb<ValueType> > >& emits);

  template <typename ValueType>
  static double StepForwardScaled(const HMMProbParams<ValueType>& params,
                                  const ValueType& sample_value,
                                  const TVector& cur,
                                  double cur_coef,
                                  TVector& alpha);

  template <typename ValueType>
  static void StepBackwardScaled(const HMMProbParams<ValueType>& params,
                                 const ValueType& sample_value,
                                 const TVector& cur,
                                 double cur_scale,
                                 TVector& beta);
};
} // namespace hmm

#endif /* HMM_H_ */
