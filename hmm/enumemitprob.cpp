#include "enumemitprob.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sstream>
#include <vector>

namespace hmm {
template<>
std::string EnumEmitProb<std::vector<size_t> >::toString() const
{
  TEnumMap::const_iterator it = _enum_map.begin();
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  for(; it!=_enum_map.end(); ++it) {
    boost::property_tree::ptree enum_value;
    const std::vector<size_t>& key = it->first;
    for(size_t i=0; i<key.size(); ++i)
      enum_value.add("key", key[i]);
    enum_value.add("value", it->second);
    pt.add_child("enum", enum_value);
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

template <>
bool EnumEmitProb<std::vector<size_t> >::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get_value("type_id");
  if(type_str!=kTypeId)
    return false;
  _enum_map.clear();
  BOOST_FOREACH(ptree::value_type &v, pt.equal_range("enum")) {
    const ptree& enum_value = v.second;
    std::vector<size_t> k;
    BOOST_FOREACH(const ptree::value_type &key, enum_value.equal_range("key"))
      k.push_back(key.second.get<size_t>(""));
    double val = enum_value.get<double>("value");
    _enum_map.insert(std::make_pair(k, val));
  }
  return true;
}

} //namespace hmm
