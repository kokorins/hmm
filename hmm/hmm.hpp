#ifndef HMM_HPP_
#define HMM_HPP_
#include "hmm.h"
#include "fixedemit.hpp"
#include <limits>
#include <numeric>
#include <functional>
#include <cstddef>
#include <algorithm>
#include <cstddef>
#undef min
#undef max

namespace hmm {
template<typename ValueType>
void HMM::ForwardScaled(const HMMProbParams<ValueType>& params,
    const IValReader<ValueType>& sample,
    std::vector<std::vector<double> >& alphas, std::vector<double>& coefs)
{
  size_t num_states = params.numStates();
  const std::vector<typename HMMProbParams<ValueType>::TEmitPtr>& emit = params.emits();
  std::vector<double> alpha(num_states);
  for (size_t i = 0; i < num_states; ++i)
    alpha[i] = params.init()[i] * emit[i]->prob(sample.get(0));
  double coef = 1.0 / std::accumulate(alpha.begin(), alpha.end(), 0.0);
  for (size_t i = 0; i < num_states; ++i)
    alpha[i] *= coef;
  alphas.reserve(sample.size());
  coefs.reserve(sample.size());
  alphas.push_back(alpha);
  coefs.push_back(coef);
  std::vector<double> cur = alpha;
  for (size_t step = 1; step < sample.size(); ++step) {
    std::swap(alpha, cur);
    coef = StepForwardScaled(params, sample.get(step), cur, coef, alpha);
    alphas.push_back(alpha);
    coefs.push_back(coef);
  }
}

template<typename ValueType>
void HMM::BackwardScaled(const HMMProbParams<ValueType>& params,
                         const IValReader<ValueType>& sample,
                         const std::vector<double>& coefs,
                         std::vector<std::vector<double> >& betas)
{
  size_t num_states = params.numStates();
  std::vector<double> beta(num_states, 1.0);
//  for (size_t i = 0; i < num_states; ++i)
//    beta[i] = 1;
  betas.resize(sample.size());
  betas.back() = beta;
  std::vector<double> cur = beta;
  for (size_t step = sample.size() - 1; step > 0; --step) {
    std::swap(beta, cur);
    StepBackwardScaled(params, sample.get(step), cur, coefs[step - 1], beta);
    betas[step - 1] = beta;
  }
}

template<typename ValueType>
std::pair<double, std::vector<size_t> >
HMM::LogViterbi(const HMMProbParams<ValueType>& params,
                const IValReader<ValueType>& sample)
{
  size_t num_states = params.numStates();
  std::vector<std::vector<double> > t = params.trans();
  for (size_t i = 0; i < t.size(); ++i)
    for (size_t j = 0; j < t[i].size(); ++j)
      t[i][j] = std::log(t[i][j]);
  const std::vector<typename HMMProbParams<ValueType>::TEmitPtr>& emit = params.emits();
  TVector delta(num_states);
  std::vector<std::vector<size_t> > phi(sample.size(), std::vector<size_t>(num_states));
  for (size_t i = 0; i < num_states; ++i) {
    double init_val = params.init()[i];
    if(init_val<std::numeric_limits<double>::min())
      init_val = std::numeric_limits<double>::min();
    double emit_val = emit[i]->prob(sample.get(0));
    if(emit_val<std::numeric_limits<double>::min())
      emit_val = std::numeric_limits<double>::min();
    delta[i] = std::log(init_val) + std::log(emit_val);
    phi.front()[i] = 0;
  }
  TVector cur = delta;
  for (size_t step = 1; step < sample.size(); ++step) {
    const ValueType& s = sample.get(step);
    std::swap(cur, delta);
    for (size_t j = 0; j < num_states; ++j) {
      double da = -1e10;
      for (size_t i = 0; i < num_states; ++i) {
        double elem = cur[i] + t[i][j];
        if (elem > da) {
          phi[step][j] = i;
          da = elem;
        }
      }
      double emit_val = emit[j]->prob(s);
      if(emit_val<std::numeric_limits<double>::min())
        emit_val = std::numeric_limits<double>::min();
      delta[j] = da + std::log(emit_val);
    }
  }
  size_t q = 0;
  double p = delta.front();
  for (size_t i = 1; i < num_states; ++i) {
    if (p < delta[i]) {
      q = i;
      p = delta[i];
    }
  }
  std::vector<size_t> res(sample.size());
  res.back() = q;
  for (size_t step = sample.size() - 1; step > 0; --step)
    res[step - 1] = phi[step][res[step]];
  return std::make_pair(p, res);
}

template<typename ValueType>
HMMProbParams<ValueType>
HMM::EstimateParameters(size_t num_states,
                        std::vector<typename HMMProbParams<ValueType>::TEmitPtr>& emits,
                        const std::vector<size_t>& states,
                        const IValReader<ValueType>& sample)
{
  HMMProbParams<ValueType> res;
  res.setNumStates(num_states);
  TVector init = EstimateInit(num_states, states, sample);
  res.swapInit(init);
  TMatrix trans = EstimateTrans<ValueType>(num_states, states, sample);
  res.swapTrans(trans);
  std::vector<std::map<ValueType, double> > sum_gammas(num_states);
  std::vector<double> sums(num_states);
  for(size_t i=0; i<states.size(); ++i) {
    sums[states[i]] += 1;
    const ValueType& s = sample.get(i);
    typename std::map<ValueType, double>::iterator it = sum_gammas[states[i]].find(s);
    if(it==sum_gammas[states[i]].end())
      sum_gammas[states[i]].insert(std::make_pair(s, 1.0));
    else
      it->second +=1.0;
  }
  for(size_t i=0; i<num_states; ++i)
    emits[i]->reestimate(sum_gammas[i], sums[i]);
  res.swapEmit(emits);
  return res;
}

template<typename ValueType>
std::vector<double> HMM::EstimateInit(size_t num_states,
                                      const std::vector<size_t>& states,
                                      const IValReader<ValueType>&)
{
  std::vector<double> res(num_states);
  for(size_t i=0; i<states.size(); ++i)
    res[states[i]]+=1.0;
  for(size_t i=0; i<res.size(); ++i)
    res[i]/=states.size();
  return res;
}

template<typename ValueType>
std::vector<std::vector<double> >
HMM::EstimateTrans(size_t num_states,
                   const std::vector<size_t>& states,
                   const IValReader<ValueType>& sample)
{
  size_t len = states.size();
  if(sample.size()<len)
    len = sample.size();
  std::vector<std::vector<double> > trans(num_states, std::vector<double>(num_states));
  for (size_t i = 1; i < len; ++i)
    ++trans[states[i-1]][states[i]];
  for (size_t i = 0; i < num_states; ++i) {
    double sum = std::accumulate(trans[i].begin(), trans[i].end(), 0.0);
    if (sum > 0)
      for (size_t j = 0; j < num_states; ++j)
        trans[i][j] /= sum;
  }
  return trans;
}

template<typename ValueType>
HMMProbParams<ValueType> HMM::LogBaumWelchStep(const HMMProbParams<ValueType>& params,
                                               const IValReader<ValueType>& sample)
{
  size_t num_states = params.numStates();
  HMMProbParams<ValueType> res;
  res.setNumStates(num_states);
  TMatrix alphas;
  TVector coefs;
  ForwardScaled(params, sample, alphas, coefs);
  TMatrix betas;
  BackwardScaled(params, sample, coefs, betas);
  const TMatrix& t = params.trans();
  std::vector<typename HMMProbParams<ValueType>::TEmitPtr> emits = params.emits();
  TMatrix chi(num_states, std::vector<double>(num_states));
  TMatrix trans_nom(num_states, std::vector<double>(num_states));
  std::vector<std::map<ValueType, double> > emits_nom(num_states);
  TVector denom(num_states);
  for (size_t k = 1; k < sample.size(); ++k) {
    const ValueType& s = sample.get(k);
    for (size_t i = 0; i < num_states; ++i) {
      double gamma = 0;
      for (size_t j = 0; j < num_states; ++j) {
        chi[i][j] = alphas[k - 1][i] * t[i][j] * emits[j]->prob(s) * betas[k][j];
        trans_nom[i][j] += chi[i][j];
        gamma += chi[i][j];
      }
      denom[i] += gamma;
      emits_nom[i][s] += gamma;
    }
  }

  double su = std::accumulate(denom.begin(), denom.end(), 0.0);
  std::vector<double> init(num_states);
  for (size_t i = 0; i < num_states; ++i)
    init[i] = denom[i]/su;
  res.swapInit(init);

  TMatrix trans(num_states, std::vector<double>(num_states));
  for (size_t i = 0; i < num_states; ++i)
    for (size_t j = 0; j < num_states; ++j)
      trans[i][j] = trans_nom[i][j] / denom[i];
  res.swapTrans(trans);

  CreateEmitters(emits_nom, denom, emits);
  res.swapEmit(emits);

  return res;
}

template<typename ValueType>
void HMM::CreateEmitters(const std::vector<std::map<ValueType, double> >& emit_sum_gammas,
                         const std::vector<double>& sum_gammas,
                         std::vector<boost::shared_ptr<ITypedEmitProb<ValueType> > >& emits)
{
  for (size_t i = 0; i < emits.size(); ++i)
    emits[i]->reestimate(emit_sum_gammas[i], sum_gammas[i]);
}

template<typename ValueType>
double HMM::StepForwardScaled(const HMMProbParams<ValueType>& params,
                              const ValueType& sample_value,
                              const std::vector<double>& cur,
                              double cur_coef, std::vector<double>& alpha)
{
  for (size_t i = 0; i < params.numStates(); ++i) {
    alpha[i] = 0;
    for (size_t j = 0; j < params.numStates(); ++j)
      alpha[i] += (cur[j] * params.trans()[i][j] * params.emits()[j]->prob(sample_value));
  }
  double coef = 1.0 / std::accumulate(alpha.begin(), alpha.end(), 0.0);
  for (size_t i = 0; i < alpha.size(); ++i)
    alpha[i] *= coef;
  return coef;
}

template<typename ValueType>
void HMM::StepBackwardScaled(const HMMProbParams<ValueType>& params,
                             const ValueType& sample_value,
                             const std::vector<double>& cur, double coef,
                             std::vector<double>& beta)
{
  for (size_t i = 0; i < params.numStates(); ++i) {
    beta[i] = 0;
    for (size_t j = 0; j < params.numStates(); ++j)
      beta[i] += (cur[j] * params.trans()[i][j] * params.emits()[j]->prob(sample_value));
    beta[i] *= coef;
  }
}

} // namespace hmm
#endif /* HMM_HPP_ */
