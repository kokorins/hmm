#ifndef EMITFACTORY_HPP_
#define EMITFACTORY_HPP_
#include "emitfactory.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace hmm {
template <typename ValueType>
ITypedEmitProb<ValueType> *ListEmitFactory<ValueType>::create(const std::string &json) const
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return 0;
  std::string type_str = pt.get_value("type_id");
  for(size_t i=0; i<_types.size(); ++i) {
    if(type_str == _types[i]->type()) {
      ITypedEmitProb<ValueType> * res = _types[i]->createClone();
      res->fromString(json);
      return res;
    }
  }
  return 0;
}

template <typename ValueType>
void ListEmitFactory<ValueType>::add(ITypedEmitProb<ValueType> *tp)
{
  _types.push_back(boost::shared_ptr<ITypedEmitProb<ValueType> >(tp));
}
} //namespace hmm
#endif
