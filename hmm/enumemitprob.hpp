#ifndef ENUMEMITPROB_HPP
#define ENUMEMITPROB_HPP
#include "enumemitprob.h"
#include <finiteiterator/finiterange.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <sstream>
namespace hmm {
template <typename ValueType>
const std::string EnumEmitProb<ValueType>::kTypeId = "statalgos.hmm.EnumEmitProb";
template <typename ValueType>
double EnumEmitProb<ValueType>::prob(const ValueType& val)const {
  typename TEnumMap::const_iterator it = _enum_map.find(val);
  if(it == _enum_map.end())
    return 0;
  return it->second;
}

template <typename ValueType>
void EnumEmitProb<ValueType>::reestimate(const std::map<ValueType, double>& emit_sum_gamma,
                                         double sum_gamma)
{
  typename EnumEmitProb<ValueType>::TEnumMap enum_map;
  typename std::map<ValueType, double>::const_iterator it = emit_sum_gamma.begin();
  for (;it!= emit_sum_gamma.end(); ++it)
    enum_map.insert(std::make_pair(it->first, it->second / sum_gamma));
  _enum_map.swap(enum_map);
}

template <typename ValueType>
const std::string& EnumEmitProb<ValueType>::type()const {
  return kTypeId;
}

template <typename ValueType>
std::string EnumEmitProb<ValueType>::toString() const
{
  typename TEnumMap::const_iterator it = _enum_map.begin();
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  for(; it!=_enum_map.end(); ++it) {
    boost::property_tree::ptree enum_value;
    enum_value.put("key", it->first);
    enum_value.put("value", it->second);
    pt.add_child("enum", enum_value);
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}


template <typename ValueType>
bool EnumEmitProb<ValueType>::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get_value("type_id");
  if(type_str!=kTypeId)
    return false;
  BOOST_FOREACH(const ptree::value_type &v, pt.get_child("enum")) {
    _enum_map.insert(std::make_pair(v.second.get<ValueType>("key"), v.second.get<double>("value")));
  }
  return true;
}

template <typename ValueType>
ITypedEmitProb<ValueType> *EnumEmitProb<ValueType>::createClone() const
{
  return new EnumEmitProb<ValueType>(*this);
}

template<typename ValueType>
EnumEmitProb<ValueType>
EnumEmitProb<ValueType>::IndependentAsDependent(const FixedEmitProb<ValueType> & emitter)
{
  typename EnumEmitProb<ValueType>::TEnumMap enum_map;
  std::vector<size_t> sizes(emitter.probs().size());
  std::transform(emitter.probs().begin(), emitter.probs().end(),
                 sizes.begin(), std::mem_fun_ref(&std::vector<double>::size));
  FiniteRange fr(sizes);
  for(FiniteIterator it = fr.begin(); it!=fr.end(); ++it) {
    ValueType vt(it.state());
    double val;
    if((val = emitter.prob(vt))>1e-15)
      enum_map.insert(std::make_pair(vt, val));
  }
  return EnumEmitProb<ValueType>(enum_map);
}

} //namespace hmm
#endif // ENUMEMITPROB_HPP
