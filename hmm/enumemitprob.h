#ifndef ENUMEMITPROB_H
#define ENUMEMITPROB_H
#include "emitter.h"
#include <string>
#include <map>

namespace hmm {
  template<typename ValueType>
  class FixedEmitProb;
/**
 * \class EnumEmitProb emitter.h "hmm/emitter.h"
 * \brief multivariate abstract probabilities container for dependent variables.
 *
 * Specific implementation for "classic" finite discrete set of
 * available observation values.
 */
template <typename ValueType>
class EnumEmitProb : public ITypedEmitProb<ValueType> {
public:
  typedef std::map<ValueType, double> TEnumMap;
public:
  EnumEmitProb() {} ///< For loading from file
  explicit EnumEmitProb(const TEnumMap& enum_map):_enum_map(enum_map) {}
  /**
   * \param val ValueType is restricted as in Alphabet() function
   * \return the probability to get the value
   */
  virtual double prob(const ValueType& val)const;
  virtual void reestimate(const std::map<ValueType, double>& emit_sum_gamma,
                          double sum_gamma);
  virtual const std::string& type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual ITypedEmitProb<ValueType>* createClone()const;
  const TEnumMap& enumMap()const {
    return _enum_map;
  }
public:
  /**
   * \brief convert independent format back to dependent
   * \param emitter input independent sample
   * \return dependent variables probabilities for all non zero probabilities
   */
   static EnumEmitProb<ValueType>
   IndependentAsDependent(const FixedEmitProb<ValueType> & emitter);
public:
  static const std::string kTypeId;
private:
  TEnumMap _enum_map;
};
template<>
std::string EnumEmitProb<std::vector<size_t> >::toString() const;
template <>
bool EnumEmitProb<std::vector<size_t> >::fromString(const std::string &json);
} //namespace hmm
#endif // ENUMEMITPROB_H
