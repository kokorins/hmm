#ifndef FIXEDEMIT_H_
#define FIXEDEMIT_H_
#include "emitter.h"
#include "hmmparams.h"
#include <vector>
#include <cstddef>
#include <set>
/// \file fixedemit.h
namespace hmm {
  template<typename ValueType>
  class EnumEmitProb;
 /**
  * \class FixedEmitProb fixedemit.h "hmm/fixedemit.h"
  * \brief Independent discrete probabilities emitter
  *
  * All variables are discrete 0,..,n-1
  * ValueType is vector of size_t by default 
  * ValueType should implement:
  *   - size_t operator[](size_t)
  *   - size_t size()const
  */
template <typename ValueType>
class FixedEmitProb : public IEmitProb<ValueType> {
public:
  FixedEmitProb() {} ///<For loading from file
 /**
  * \brief Constructor of FixedEmitProb
  * \param probs contains a vector of probabilities for each variable. 
  *        It possible to have different lengths for different variables
  */
  explicit FixedEmitProb(const std::vector<std::vector<double> >& probs):_probs(probs){}
 /**
  * \brief implementation of final probability calculation.
  * \param val is supposed to be a vector of size_t, i-th value is a index of i-th variable
  * \return production over the probabilities for all variables
  */
  virtual double prob(const ValueType& val)const;
  virtual void reestimate(const std::map<ValueType, double>& emit_sum_gamma,
                          double sum_gamma);

  const std::vector<std::vector<double> >& probs()const {return _probs;}
public:
  /**
   * \brief Estimates parameters by training set as
   * if variables are finite independently distributed
   *
   * \sa EstimateParameters()
   */
  static HMMProbParams<ValueType>
  EstimateIndependent(size_t num_states,
                      const std::vector<size_t>& states,
                      const IValReader<ValueType>& sample,
                      const std::vector<size_t>& sizes);
  /**
   * \brief Treat dependent parameters as independent
   * \param emitter input dependent format \sa EnumEmitProb
   * \param alphabet alphabet of input emitter. TODO: remove
   * \param sizes boundaries of a independent discrete variable
   */
   static FixedEmitProb<ValueType>
   DependentAsIndependent(const EnumEmitProb<ValueType>& emitter,
                          const std::set<ValueType>& alphabet,
                          const ValueType& sizes);
private:
  std::vector<std::vector<double> > _probs;
};

} // namespace hmm
#endif // FIXEDEMIT_H_
