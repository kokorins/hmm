#ifndef HMMPARAMS_HPP_
#define HMMPARAMS_HPP_
#include "hmmparams.h"
#include <set>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

namespace hmm {
template <typename ValueType>
const std::string HMMProbParams<ValueType>::kTypeId = "statalgos.hmm.HMMProbParams.1";

template <typename ValueType>
void HMMProbParams<ValueType>::setNumStates(size_t num_states) {
  _num_states = num_states;
}

template <typename ValueType>
void HMMProbParams<ValueType>::swapInit(std::vector<double>& init) {
  std::swap(init, _init);
}

template <typename ValueType>
void HMMProbParams<ValueType>::swapTrans(std::vector<std::vector<double> >& trans) {
  std::swap(trans, _trans);
}

template <typename ValueType>
void HMMProbParams<ValueType>::swapEmit(std::vector<TEmitPtr>& emit) {
  std::swap(emit, _emit);
}

template <typename ValueType>
size_t HMMProbParams<ValueType>::numStates()const {
  return _num_states;
}

template <typename ValueType>
const std::vector<double>&
HMMProbParams<ValueType>::init()const {
  return _init;
}

template <typename ValueType>
const std::vector<std::vector<double> >&
HMMProbParams<ValueType>::trans()const {
  return _trans;
}

template <typename ValueType>
const std::vector<typename HMMProbParams<ValueType>::TEmitPtr>&
HMMProbParams<ValueType>::emits()const {
  return _emit;
}

template <typename ValueType>
const std::string &HMMProbParams<ValueType>::type() const
{
  return kTypeId;
}

template <typename ValueType>
std::string HMMProbParams<ValueType>::toString() const
{
  using boost::property_tree::ptree;
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("num_states", _num_states);
  for(size_t i=0; i<_num_states; ++i)
    pt.add("init", _init[i]);
  for(size_t i=0; i<_num_states; ++i) {
    for(size_t j=0; j<_num_states; ++j) {
      pt.add("trans", _trans[i][j]);
    }
  }
  for(size_t i=0; i<_emit.size(); ++i) {
    std::string emit_json = _emit[i]->toString();
    ptree emit_pt;
    std::stringstream ss(emit_json);
    boost::property_tree::json_parser::read_json(ss, emit_pt);
    emit_pt.put("idx", i);
    pt.add_child("emit", emit_pt);
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

template <typename ValueType>
bool HMMProbParams<ValueType>::fromString(const std::string &json, const IEmitFactory<ValueType>& emit_factory)
{
  try {
    using boost::property_tree::ptree;
    std::stringstream ss(json);
    ptree pt;
    boost::property_tree::json_parser::read_json(ss, pt);
    ptree::assoc_iterator it = pt.find("type_id");
    if(it==pt.not_found())
      return false;
    std::string type_str = pt.get<std::string>("type_id");
    if(type_str!=kTypeId)
      return false;
    _num_states = pt.get<size_t>("num_states");
    _init.resize(_num_states);
    size_t idx = 0;
    BOOST_FOREACH(const ptree::value_type &v, pt.equal_range("init")) {
      _init[idx] = v.second.get<double>("");
      ++idx;
    }
    _trans = std::vector<std::vector<double> >(_num_states, std::vector<double>(_num_states));
    idx=0;
    BOOST_FOREACH(const ptree::value_type &v, pt.equal_range("trans")) {
      _trans[idx/_num_states][idx%_num_states] = v.second.get<double>("");
      ++idx;
    }

    _emit.clear();

    BOOST_FOREACH(const ptree::value_type &v, pt.equal_range("emit")) {
      std::stringstream ss;
      boost::property_tree::json_parser::write_json(ss, v.second);
      ITypedEmitProb<ValueType>* tp = emit_factory.create(ss.str());
      if(tp)
        _emit.push_back(boost::shared_ptr<ITypedEmitProb<ValueType> >(tp));
      else // if not all types could be recognized nothing to do
        return false;
    }
    return true;
  }
  catch(boost::property_tree::json_parser::json_parser_error&) {
    return false;
  }
}

template <typename ValueType>
std::ostream& operator<<(std::ostream& out,
                         const HMMProbParams<ValueType>& params)
{
  return out<<params.toString();
}
} // namespace hmm
#endif /* HMMPARAMS_HPP_ */
