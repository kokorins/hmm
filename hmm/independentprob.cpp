#include "independentprob.h"
#include "emitfactory.hpp"
#include <numeric>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

namespace hmm {
const std::string IndependentProb::kTypeId = "statalgos.hmm.Independent";

IndependentProb::IndependentProb() {}

IndependentProb::IndependentProb(const std::vector<EmitProbPtr> &emitters) : _emitters(emitters) {}

double IndependentProb::prob(const std::vector<ValueType> &val) const
{
  double res = 1;
  for(size_t i=0; i<_emitters.size(); ++i)
    res *= _emitters[i]->prob(val[i]);
  return res;
}

void IndependentProb::reestimate(const std::map<std::vector<ValueType>, double>& emit_sum_gamma, double sum_gamma)
{
  for(size_t i=0; i<_emitters.size(); ++i) {
    std::map<double, double> emit_part_sum_gamma;
    std::map<std::vector<double>, double>::const_iterator it = emit_sum_gamma.begin();
    for(; it!= emit_sum_gamma.end(); ++it) {
      std::map<double, double>::iterator pit = emit_part_sum_gamma.find(it->first[i]);
      if(pit==emit_part_sum_gamma.end())
        emit_part_sum_gamma.insert(std::make_pair(it->first[i], it->second));
      else
        pit->second += it->second;
    }
    _emitters[i]->reestimate(emit_part_sum_gamma, sum_gamma);
  }
}

const std::string &IndependentProb::type() const
{
  return kTypeId;
}

std::string IndependentProb::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  for(size_t i=0; i<_emitters.size(); ++i) {
    std::string emit_json = _emitters[i]->toString();
    std::stringstream ss(emit_json);
    ptree emit_pt;
    //TODO check why removed
//    boost::property_tree::json_parser::read_json(ss, emit_pt);
    emit_pt.put("idx", i);
    pt.add_child("emit", emit_pt);
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool IndependentProb::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get_value("type_id");
  if(type_str!=kTypeId)
    return false;
  _emitters.clear();
  BOOST_FOREACH(ptree::value_type &v, pt.equal_range("emit")) {
    const ptree& emit_pt = v.second;
    std::stringstream ss;
    boost::property_tree::json_parser::write_json(ss, emit_pt);
    ITypedEmitProb<double>* tp = _emit_fact->create(ss.str());
    if(tp)
      _emitters.push_back(EmitProbPtr(tp));
    else
      return false;
  }
  return true;
}

ITypedEmitProb<std::vector<ValueType> > *IndependentProb::createClone() const
{
  std::vector<EmitProbPtr> emitters;
  for(size_t i=0; i<_emitters.size(); ++i)
    emitters.push_back(EmitProbPtr(_emitters[i]->createClone()));
  return new IndependentProb(emitters);
}

void IndependentProb::setEmitFactory(IEmitFactory<ValueType>* emit_fact)
{
  _emit_fact.reset(emit_fact);
}

void IndependentProb::swapEmitters(std::vector<EmitProbPtr> &emitters)
{
  std::swap(_emitters, emitters);
}

const std::vector<IndependentProb::EmitProbPtr>& IndependentProb::emitters() const
{
  return _emitters;
}
} //namespace hmm
