#ifndef FIXEDEMIT_HPP_
#define FIXEDEMIT_HPP_
#include "fixedemit.h"
#include "enumemitprob.h"
#include "hmm.h"
#include <numeric>
namespace hmm {
template <typename ValueType>
double FixedEmitProb<ValueType>::prob(const ValueType& idxs) const
{
  double val = 1;
  for(size_t i=0; i<idxs.size(); ++i)
    val *= _probs[i][idxs[i]];
  return val;
}

template<typename ValueType>
void FixedEmitProb<ValueType>::reestimate(const std::map<ValueType, double> &emit_sum_gamma,
                                          double sum_gamma)
{
  std::vector<std::vector<double> > sums(_probs.size());
  for(size_t i=0; i<_probs.size(); ++i)
    sums.resize(_probs[i].size());
  typename std::map<ValueType, double>::const_iterator it = emit_sum_gamma.begin();
  for(; it!=emit_sum_gamma.end(); ++it)
    for(size_t i=0; i<it->first.size(); ++i)
      sums[i][it->first[i]]+= it->second;
  for(size_t i=0; i<sums.size(); ++i)
    for(size_t j=0;j<sums[i].size(); ++j)
      _probs[i][j] = sums[i][j]/sum_gamma;
}

template<typename ValueType>
HMMProbParams<ValueType>
FixedEmitProb<ValueType>::EstimateIndependent(size_t num_states,
                                              const std::vector<size_t>& states,
                                              const IValReader<ValueType>& sample,
                                              const std::vector<size_t>& sizes)
{
  size_t len = states.size();
  if(sample.size()<len)
    len = sample.size();
  HMMProbParams<ValueType> res;
  res.setNumStates(num_states);
  std::vector<double> init =
      HMM::EstimateInit<ValueType>(num_states, states, sample);
  res.swapInit(init);
  std::vector<std::vector<double> > trans =
      HMM::EstimateTrans<ValueType>(num_states, states, sample);
  res.swapTrans(trans);
  std::vector<std::vector<std::vector<double> > > emit_vector(num_states,
                                                              std::vector<std::vector<double> >(sizes.size()));
  for (size_t i=0; i < num_states; ++i)
    for(size_t j=0; j < sizes.size(); ++j)
      emit_vector[i][j].resize(sizes[j]);
  for (size_t i = 0; i < len; ++i) {
    const ValueType& s = sample.get(i);
    for(size_t j=0; j<sizes.size(); ++j)
      ++emit_vector[states[i]][j][s[j]];
  }
  for(size_t i=0; i < emit_vector.size(); ++i) {
    for(size_t j=0; j<sizes.size(); ++j) {
      double sum = std::accumulate(emit_vector[i][j].begin(), emit_vector[i][j].end(), 0.0);
      if(std::fabs(sum)<std::numeric_limits<double>::min())
        continue;
      for(size_t k=0; k<sizes[j]; ++k)
        emit_vector[i][j][k]/=sum;
    }
  }
  std::vector<boost::shared_ptr<IEmitProb<ValueType> > > emits(num_states);
  for (size_t i = 0; i < num_states; ++i)
    emits[i].reset(new FixedEmitProb<ValueType>(emit_vector[i]));
  res.swapEmit(emits);
  return res;
}

template<typename ValueType>
FixedEmitProb<ValueType>
FixedEmitProb<ValueType>::DependentAsIndependent(const EnumEmitProb<ValueType>& emitter,
                                                 const std::set<ValueType>& alphabet,
                                                 const ValueType& sizes)
{
  std::vector<std::vector<double> > emit_prob(sizes.size());
  for(size_t i=0; i<emit_prob.size(); ++i)
    emit_prob[i].resize(sizes[i]);
  for(typename std::set<ValueType>::const_iterator it = alphabet.begin();
      it!= alphabet.end(); ++it) {
    const ValueType& x = *it;
    double val = emitter.prob(*it);
    for(size_t i=0; i<x.size(); ++i)
      emit_prob[i][x[i]]+=val;
  }
  for(size_t i=0; i<emit_prob.size(); ++i) {
    double sum = std::accumulate(emit_prob[i].begin(), emit_prob[i].end(), 0.0);
    std::transform(emit_prob[i].begin(), emit_prob[i].end(), emit_prob[i].begin(),
                   std::bind2nd(std::divides<double>(),sum));
  }
  return FixedEmitProb<ValueType>(emit_prob);
}
} // namespace hmm
#endif // FIXEDEMIT_HPP_
