#ifndef HMMMANAGER_H_
#define HMMMANAGER_H_
#include <vector>
#include <cstddef>
/// \file hmmmanager.h
namespace hmm {
/**
 * Encapsulation for object reading/calculation.
 */
template <typename ValueType>
class IValReader {
public:
  /**
   * Get an observation at the moment idx
   * \param idx observation of interest
   */
  virtual const ValueType& get(size_t idx)const=0;
  /**
   * \return number of timepoints
   */
  virtual size_t size()const=0;
  virtual ~IValReader() {}
};

/**
 * Simple vector wrapper implementation for small number of observation and
 * for testing purposes.
 */
template <typename ValueType>
class ValReader : public IValReader<ValueType> {
public:
  explicit ValReader(const std::vector<ValueType>& vals):_vals(vals){}
  virtual const ValueType& get(size_t idx)const {
    return _vals[idx];
  }
  virtual size_t size()const{
    return _vals.size();
  }
private:
  std::vector<ValueType> _vals;
};
} // namespace hmm

#endif /* HMMMANAGER_H_ */
