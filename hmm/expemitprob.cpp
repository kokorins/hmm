#include "expemitprob.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace hmm {
const std::string ExpEmitProb::kTypeId = "statalgos.hmm.ExpEmitProb.1";
double ExpEmitProb::prob(const double &val) const
{
  if(val<0)
    return 0;
  return _lambda*std::exp(-_lambda*val);
}

void ExpEmitProb::reestimate(const std::map<double, double> &emit_sum_gamma, double sum_gamma)
{
  _lambda = 0;
  std::map<double, double>::const_iterator it = emit_sum_gamma.begin();
  for(;it!=emit_sum_gamma.end(); ++it)
    _lambda += it->first*it->second;
  if(sum_gamma>0)
    _lambda /=sum_gamma;
  else
    _lambda = 1;
  _lambda = 1./_lambda;
}

const std::string &ExpEmitProb::type() const
{
  return kTypeId;
}

std::string ExpEmitProb::toString() const
{
  boost::property_tree::ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("lambda", _lambda);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool ExpEmitProb::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get_value("type_id");
  if(type_str!=kTypeId)
    return false;
  _lambda  = pt.get<double>("lambda");
  return true;
}

double ExpEmitProb::lambda() const
{
  return _lambda;
}

ITypedEmitProb<double> *ExpEmitProb::createClone() const
{
  return new ExpEmitProb(*this);
}
} //namespace hmm
