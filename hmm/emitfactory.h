#ifndef EMITFACTORY_H
#define EMITFACTORY_H
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
namespace hmm {
template <typename TValueType> class ITypedEmitProb;
/**
 * Interface for adding different types of emitters according lists
 */
template <typename ValueType>
class IEmitFactory {
public:
  /**
   * Returns zero if any error inside
   */
  virtual ITypedEmitProb<ValueType>* create(const std::string& json)const = 0;
  virtual ~IEmitFactory() {}
};

template <typename ValueType>
class ListEmitFactory : public IEmitFactory<ValueType> {
public:
  virtual ITypedEmitProb<ValueType>* create(const std::string& json)const;
  void add(ITypedEmitProb<ValueType>* tp);
private:
  std::vector<boost::shared_ptr<ITypedEmitProb<ValueType> > > _types;
};
} //namespace hmm
#endif // EMITFACTORY_H
