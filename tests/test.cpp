#define BOOST_TEST_MODULE HmmTests
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <hmm/hmm.h>
#include <hmm/enumemitprob.hpp>
#include <hmm/emitfactory.hpp>
#include <hmm/normalemitprob.h>
#include <hmm/hmmparams.hpp>
#include <hmm/hmm.hpp>
#include <hmm/expemitprob.h>
#include <hmm/independentprob.h>

#include <boost/random.hpp>
#include <algorithm>
#include <functional>
#include <numeric>
#include <boost/test/unit_test.hpp>
#include <boost/tuple/tuple.hpp>

struct iota {
  int idx_;
public:
  iota(int idx):idx_(idx){}
  iota():idx_(0){}
  int operator()() {
    return idx_++;
  }
};


BOOST_AUTO_TEST_CASE(EnumProbability)
{
  using namespace hmm;
  std::map<int, double> enum_map;
  enum_map.insert(std::make_pair(0, 0.5));
  enum_map.insert(std::make_pair(1, 0.3));
  enum_map.insert(std::make_pair(2, 0.2));
  EnumEmitProb<int> eep(enum_map);
  BOOST_CHECK_CLOSE(eep.prob(0), 0.5, 0.1);
  BOOST_CHECK_CLOSE(eep.prob(1), 0.3, 0.1);
  BOOST_CHECK_CLOSE(eep.prob(2), 0.2, 0.1);
}

BOOST_AUTO_TEST_CASE(EnumReestimate)
{
  using namespace hmm;
  std::map<int, double> enum_map;
  enum_map.insert(std::make_pair(0, 0.5));
  enum_map.insert(std::make_pair(1, 0.3));
  enum_map.insert(std::make_pair(2, 0.2));
  EnumEmitProb<int> eep(enum_map);
  std::map<int, double> new_enum_map;
  double factor = 2.0;
  new_enum_map.insert(std::make_pair(0, factor/3));
  new_enum_map.insert(std::make_pair(1, factor/3));
  new_enum_map.insert(std::make_pair(2, factor/3));
  eep.reestimate(new_enum_map, factor);
  std::map<int, double>::const_iterator it = eep.enumMap().begin();
  for(; it!=eep.enumMap().end(); ++it)
    BOOST_CHECK_CLOSE(it->second, new_enum_map.find(it->first)->second/factor, .1);
}

BOOST_AUTO_TEST_CASE(NormalProbability)
{
  using namespace hmm;
  NormalEmitProb nep(0,1);
  BOOST_CHECK_CLOSE(nep.prob(0), 0.3989423, 0.1);
  BOOST_CHECK_CLOSE(nep.prob(1), 0.2419707, 0.1);
  BOOST_CHECK_CLOSE(nep.prob(-1), 0.2419707, 0.1);
  NormalEmitProb nep2(1,2);
  BOOST_CHECK_CLOSE(nep2.prob(0), 0.2196956, 0.1);
  BOOST_CHECK_CLOSE(nep2.prob(1), 0.2820948, 0.1);
  BOOST_CHECK_CLOSE(nep2.prob(-1), 0.1037769, 0.1);
}

BOOST_AUTO_TEST_CASE(NormalReestimate)
{
  using namespace hmm;
  NormalEmitProb nep(0,1);
  std::map<double, double> emit_sum_gamma;
  boost::minstd_rand urnd;
  boost::normal_distribution<> ndist(1,2);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen(urnd, ndist);
  size_t len = 5000;
  std::vector<double> nrand(len);
  std::generate(nrand.begin(), nrand.end(), ngen);
  double mean = std::accumulate(nrand.begin(), nrand.end(), 0.0)/len;
  double var = std::inner_product(nrand.begin(), nrand.end(), nrand.begin(), 0.0)/len;
  var -= mean*mean;
  for(size_t i=0; i<nrand.size(); ++i)
    emit_sum_gamma.insert(std::make_pair(nrand[i], 1.0));
  nep.reestimate(emit_sum_gamma, (double)len);
  BOOST_CHECK_CLOSE(nep.mean(), mean, 1);
  BOOST_CHECK_CLOSE(nep.var(), var, 1);
}

BOOST_AUTO_TEST_CASE(NormalViterbi)
{
  using namespace hmm;
  double logp;
  std::vector<size_t> states;
  boost::minstd_rand urnd;
  boost::normal_distribution<> ndist1(1,2);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen1(urnd, ndist1);
  boost::normal_distribution<> ndist2(0,1);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen2(urnd, ndist2);
  size_t len = 1000;
  std::vector<double> ss(len);
  std::generate(ss.begin(), ss.end(), ngen1);
  std::generate(ss.begin()+ss.size()/2, ss.end(), ngen2);
  HMMProbParams<double> params;
  ValReader<double> sample(ss);
  params.setNumStates(2);
  std::vector<double> init(2,0.5);
  params.swapInit(init);
  std::vector<std::vector<double> >trans(2, std::vector<double>(2));
  trans[0][0] = trans[1][1] = 1-1./len;
  trans[0][1] = trans[1][0] = 1./len;
  params.swapTrans(trans);
  std::vector<HMMProbParams<double>::TEmitPtr> emits(2);
  emits[0].reset(new NormalEmitProb(1,2));
  emits[1].reset(new NormalEmitProb(0,1));
  params.swapEmit(emits);
  boost::tie(logp, states) = HMM::LogViterbi<double>(params, sample);
  std::vector<size_t> exp_states(len);
  std::fill(exp_states.begin()+exp_states.size()/2, exp_states.end(), 1);
  size_t count = 0;
  for(size_t i=0; i<exp_states.size(); ++i)
    if(states[i]==exp_states[i])
      ++count;
  BOOST_CHECK_CLOSE(1.*count, 1.*len, 1);
}

BOOST_AUTO_TEST_CASE(NormalBaumWelch)
{
  using namespace hmm;
  double logp;
  std::vector<size_t> states;
  boost::minstd_rand urnd;
  boost::normal_distribution<> ndist1(2,5);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen1(urnd, ndist1);
  boost::normal_distribution<> ndist2(0,5);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen2(urnd, ndist2);
  size_t len = 10000;
  std::vector<double> ss(len);
  std::generate(ss.begin(), ss.end(), ngen1);
  std::generate(ss.begin()+ss.size()/2, ss.end(), ngen2);
  HMMProbParams<double> params;
  ValReader<double> sample(ss);
  params.setNumStates(2);
  std::vector<double> init(2,0.5);
  params.swapInit(init);
  std::vector<std::vector<double> >trans(2, std::vector<double>(2));
  trans[0][0] = trans[1][1] = 1-1./len;
  trans[0][1] = trans[1][0] = 1./len;
  params.swapTrans(trans);
  std::vector<HMMProbParams<double>::TEmitPtr> emits(2);
  emits[0].reset(new NormalEmitProb(1,2));
  emits[1].reset(new NormalEmitProb(0,1));
  params.swapEmit(emits);
  boost::tie(logp, states) = HMM::LogViterbi<double>(params, sample);
  std::vector<size_t> exp_states(len);
  std::fill(exp_states.begin()+exp_states.size()/2, exp_states.end(), 1);
  size_t count = 0;
  for(size_t i=0; i<exp_states.size(); ++i)
    if(states[i]==exp_states[i])
      ++count;

  HMMProbParams<double> params_new = params;
  double logp_new;
  std::vector<size_t> states_new;

  for(int i=0; i<=5; ++i) {
    params_new = HMM::LogBaumWelchStep<double>(params, sample);
    boost::tie(logp_new, states_new) = HMM::LogViterbi<double>(params, sample);
//    std::cout<<logp_new<<std::endl;
  }

  size_t count_new = 0;
  for(size_t i=0; i<exp_states.size(); ++i)
    if(states_new[i]==exp_states[i])
      ++count_new;
//  std::cout<<logp<<" "<<logp_new<<std::endl;
//  std::cout<<count<<" "<<count_new<<std::endl;
  BOOST_CHECK_GE(logp_new, logp);
  BOOST_CHECK_GE(count_new, count);
}

BOOST_AUTO_TEST_CASE(NormalEstimate)
{
  using namespace hmm;
  boost::minstd_rand urnd;
  boost::normal_distribution<> ndist1(2,5);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen1(urnd, ndist1);
  boost::normal_distribution<> ndist2(0,5);
  boost::variate_generator<boost::minstd_rand&, boost::normal_distribution<> > ngen2(urnd, ndist2);
  size_t len = 10000;
  std::vector<double> ss(len);
  std::generate(ss.begin(), ss.end(), ngen1);
  std::generate(ss.begin()+ss.size()/2, ss.end(), ngen2);
  std::vector<double> exp_init(2,0.5);
  std::vector<std::vector<double> > exp_trans(2, std::vector<double>(2));
  exp_trans[0][0] = 1-2./len;
  exp_trans[0][1] = 2./len;
  exp_trans[1][0] = 0;
  exp_trans[1][1] = 1;
  std::vector<HMMProbParams<double>::TEmitPtr> emits(2);
  emits[0].reset(new NormalEmitProb(2,5));
  emits[1].reset(new NormalEmitProb(0,5));
  ValReader<double> sample(ss);
  std::vector<size_t> states(len);
  std::fill(states.begin()+states.size()/2, states.end(), 1);
  HMMProbParams<double> params = HMM::EstimateParameters<double>(2, emits, states, sample);
  BOOST_CHECK_EQUAL(params.numStates(),2);
  BOOST_CHECK_EQUAL_COLLECTIONS(params.init().begin(), params.init().end(), exp_init.begin(), exp_init.end());
  for(size_t i=0; i<exp_trans.size(); ++i)
    for(size_t j=0; j<exp_trans[i].size(); ++j)
      BOOST_CHECK_CLOSE(params.trans()[i][j], exp_trans[i][j], 1);
}

BOOST_AUTO_TEST_CASE(ExpEstimate)
{
  using namespace hmm;
  boost::minstd_rand urnd;
  boost::exponential_distribution<> edist1(1);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > egen1(urnd, edist1);
  boost::exponential_distribution<> edist2(1.8);
  boost::variate_generator<boost::minstd_rand&, boost::exponential_distribution<> > egen2(urnd, edist2);
  size_t len = 10000;
  std::vector<double> ss(len);
  std::generate(ss.begin(), ss.end(), egen1);
  std::generate(ss.begin()+ss.size()/2, ss.end(), egen2);
  HMMProbParams<double> params;
  params.setNumStates(2);
  std::vector<double> init(2,0.5);
  std::vector<double> exp_init(2,0.5);
  params.swapInit(init);
  std::vector<std::vector<double> > trans(2, std::vector<double>(2));
  trans[0][0] = trans[1][1] = 1-1./len;
  trans[0][1] = trans[1][0] = 1./len;
  std::vector<std::vector<double> > exp_trans(2, std::vector<double>(2));
  exp_trans[0][0] = 1-2./len;
  exp_trans[0][1] = 2./len;
  exp_trans[1][0] = 0;
  exp_trans[1][1] = 1;
  params.swapTrans(trans);
  std::vector<HMMProbParams<double>::TEmitPtr> emits(2);
  emits[0].reset(new ExpEmitProb(1));
  emits[1].reset(new ExpEmitProb(2));
  params.swapEmit(emits);
  double logp;
  std::vector<size_t> states;
  ValReader<double> sample(ss);
  boost::tie(logp, states) = HMM::LogViterbi<double>(params, sample);
  std::vector<size_t> exp_states(len);
  std::fill(exp_states.begin()+exp_states.size()/2, exp_states.end(), 1);
  size_t count = 0;
  for(size_t i=0; i<exp_states.size(); ++i)
    if(states[i]==exp_states[i])
      ++count;
  BOOST_CHECK_CLOSE(1.*count, 1.*len, 1);
}

BOOST_AUTO_TEST_CASE(HmmEnumJson)
{
  using namespace hmm;
  typedef std::vector<size_t> TDepFinite;
  std::map<TDepFinite, double> enum_map;
  TDepFinite elem(2);
  enum_map.insert(std::make_pair(elem, 0.5));
  elem = TDepFinite(2, 1);
  enum_map.insert(std::make_pair(elem, 0.5));
  EnumEmitProb<std::vector<size_t> > eep(enum_map);
  EnumEmitProb<std::vector<size_t> > eep_exp(enum_map);
  std::string json = eep.toString();
//  std::cout<<json<<std::endl;
  BOOST_CHECK(eep.fromString(json));
  BOOST_CHECK(eep.enumMap() == eep_exp.enumMap());
}

BOOST_AUTO_TEST_CASE(HmmDoubleJson)
{
  using namespace hmm;
  NormalEmitProb nep(0, 1), nep_exp(0,1);
  std::string json = nep.toString();
  BOOST_CHECK(nep.fromString(json));
  BOOST_CHECK_SMALL(std::fabs(nep.mean() - nep_exp.mean()), 1e-5);
  BOOST_CHECK_SMALL(std::fabs(nep.var() - nep_exp.var()), 1e-5);
  ExpEmitProb eep(1), eep_exp(1);
  json = eep.toString();
  BOOST_CHECK(eep.fromString(json));
  BOOST_CHECK_SMALL(std::fabs(eep.lambda() - eep_exp.lambda()), 1e-5);
}

BOOST_AUTO_TEST_CASE(HmmIndependentJson)
{
  using namespace hmm;
  IndependentProb ip;
  std::vector<IndependentProb::EmitProbPtr> emitters;
  emitters.push_back(IndependentProb::EmitProbPtr(new NormalEmitProb(1,2)));
  emitters.push_back(IndependentProb::EmitProbPtr(new NormalEmitProb(1,2)));
  ip.swapEmitters(emitters);
  ListEmitFactory<double>* emit_fact = new ListEmitFactory<double>();
  emit_fact->add(new NormalEmitProb());
  emit_fact->add(new ExpEmitProb());
  ip.setEmitFactory(emit_fact);
  std::string json = ip.toString();
//  std::cout<<json;
  BOOST_CHECK(ip.fromString(json));
  for(size_t i=0; i<ip.emitters().size(); ++i) {
    const NormalEmitProb& nep = dynamic_cast<const NormalEmitProb&>(*ip.emitters()[i]);
    BOOST_CHECK_SMALL(nep.mean()-1, 1e-5);
    BOOST_CHECK_SMALL(nep.var()-2, 1e-5);
  }
}

BOOST_AUTO_TEST_CASE(HmmParametersJson)
{
  using namespace hmm;
  HMMProbParams<double> pr;
  pr.setNumStates(3);
  std::vector<double> init(3, 1./3);
  pr.swapInit(init);
  std::vector<std::vector<double> > trans(3, std::vector<double>(3, 1./3));
  pr.swapTrans(trans);
  std::vector<boost::shared_ptr<ITypedEmitProb<double> > > emits;
  emits.push_back(boost::shared_ptr<ITypedEmitProb<double> >(new NormalEmitProb()));
  emits.push_back(boost::shared_ptr<ITypedEmitProb<double> >(new NormalEmitProb(1,2)));
  emits.push_back(boost::shared_ptr<ITypedEmitProb<double> >(new NormalEmitProb(2,3)));
  pr.swapEmit(emits);
  std::string json = pr.toString();
  ListEmitFactory<double> emit_factory;
  emit_factory.add(new ExpEmitProb());
  BOOST_CHECK(!pr.fromString(json, emit_factory));
  emit_factory.add(new NormalEmitProb);
  BOOST_CHECK(pr.fromString(json, emit_factory));
  BOOST_CHECK_EQUAL(pr.numStates(), 3);
  BOOST_CHECK_EQUAL(pr.init().size(), 3);
  for(size_t i=0; i<pr.init().size(); ++i) {
    BOOST_CHECK_CLOSE(pr.init()[i], 1./3, 1e-3);
  }
  BOOST_CHECK_EQUAL(pr.trans().size(), 3);
  BOOST_CHECK_EQUAL(pr.trans().back().size(), 3);
  for(size_t i=0; i<3; ++i){
    for(size_t j=0; j<3; ++j) {
      BOOST_CHECK_CLOSE_FRACTION(pr.trans()[i][j], 1./3, 1e-3);
    }
  }
  BOOST_CHECK_EQUAL(pr.emits().size(), 3);
  for(size_t i=0; i<3; ++i) {
    const NormalEmitProb& nep = dynamic_cast<const NormalEmitProb&>(*pr.emits()[i]);
    BOOST_CHECK_SMALL(nep.mean()-i, 1e-3);
    BOOST_CHECK_SMALL(nep.var()-i-1, 1e-3);
  }
}
